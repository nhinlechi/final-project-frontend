import React from 'react';

const ProfileContext = React.createContext({
    dataProfile: {},
    profilePage: "",
    setDataProfile: () => { },
    setProfilePage: () => { }
});

export default ProfileContext;