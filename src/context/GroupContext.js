import React, { useState, createContext } from "react";

export const GroupContext = createContext(null);

export const GroupProvider = ({ children }) => {
    const [listGroup, setListGroup] = useState([]);
    const [search, setSearch] = useState(""); // filter group theo text
    const [total, setTotal] = useState(0); // tổng số group đã filter hoặc không filter

    return (
        <GroupContext.Provider
            value={{
                listGrp: [listGroup, setListGroup],
                txtSearch: [search, setSearch],
                totalGroup: [total, setTotal]
            }}
        >
            {children}
        </GroupContext.Provider>
    );
};
