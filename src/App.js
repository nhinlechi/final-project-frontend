import React from "react";
import dotenv from "dotenv";
import "./App.css";
import "./assets/css/main.css";
import "./components/FontAwesomeIcons/index";
import "./components/_init";
import Login from "./views/Sign/Login/login";
import Home from "./views/Home/home";
import Logout from "./views/Sign/Logout/logout";
import Register from "./views/Sign/Register/register";
import ChangePassword from "./views/User/ChangePassword/index";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

// layouts
import UserLayout from "./layouts/User/user-layout";
import SignLayout from "./layouts/Sign/sign-layout";

// pages
import Encounter from "./views/Encounters/index";
import Message from "./views/Messages/index";
import Friend from "./views/Friends/index";
import Restaurant from "./views/Restaurants/index";
import Group from "./views/Groups/index";
import { PrivateRoute } from './utils/RouteUtils';

import ForgotPassword from "./views/Sign/ForgotPassword/forgot-password";
import ResetPassword from "./views/Sign/ResetPassword/reset-password";
import VideoCall from "./components/VideoCall/VideoCall";

dotenv.config();

// define route
const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
  <Route
    {...rest}
    render={(props) => (
      <Layout>
        <Component {...props}></Component>
      </Layout>
    )}
  ></Route>
);

function App() {



  return (
    <Router>
      <Switch>
        <PrivateRoute path="/" exact layout={UserLayout} component={Home} />
        <PrivateRoute
          path="/encounters"
          layout={UserLayout}
          component={Encounter}
        />
        <PrivateRoute
          path="/messages/"
          layout={UserLayout}
          component={Message}
        />
        <PrivateRoute
          path="/video-call"
          layout={UserLayout}
          component={VideoCall}
        />
        <PrivateRoute path="/friends" layout={UserLayout} component={Friend} />
        <PrivateRoute path="/groups" layout={UserLayout} component={Group} />

        <PrivateRoute
          path="/restaurants"
          layout={UserLayout}
          component={Restaurant}
        />

        <PrivateRoute
          path="/user/change-password"
          layout={UserLayout}
          component={ChangePassword}
        />
        <AppRoute path="/login" layout={SignLayout} component={Login} />
        <PrivateRoute path="/logout" layout={SignLayout} component={Logout} />
        <AppRoute path="/register" layout={SignLayout} component={Register} />
        <AppRoute path="/forgot-password" layout={SignLayout} component={ForgotPassword} />
        <AppRoute path="/reset-password" layout={SignLayout} component={ResetPassword} />
      </Switch>
    </Router>

  );
}

export default App;
