import React from "react";
import RestaurantCard from "./RestaurantCard";

export default function DefaultList(props) {

  return (
    <div>
      {props.defaultList && (
        <React.Fragment>
          {props.defaultList.map((restaurant) => (
              <RestaurantCard restaurantCard={restaurant} width={"12rem"} />          ))}
        </React.Fragment>
      )}
    </div>
  );
}
