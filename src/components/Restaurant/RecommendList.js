import React from "react";
import Slider from "react-slick";
import RestaurantCard from "./RestaurantCard";
import classes from "./RecommendList.module.css"
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function RecommendList(props) {
  const settings = {
    dots: true,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 5
  };

  return (
      <div className={classes.wrapper}>
        <Slider {...settings}>
       {props.recommendList &&
          props.recommendList.map((recommend) => (
              <RestaurantCard restaurantCard={recommend} />
          ))}
        </Slider>
        
      </div>
  );
}
