import React, { useContext, useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { RestaurantContext } from "../../../context/RestaurantContext";
import { getComment } from "../../../api/placeApi";
import { Row, Col } from "react-bootstrap";
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIosRoundedIcon from "@material-ui/icons/ArrowBackIosRounded";
import Review from "./Review";
import Info from "./Info";
import Maps from "./Maps";
import PageLoading from "../../PageLoading/pageLoading";
import Tag from "../../Tag/index";
import { restaurantInfoTabs } from "../../../utils/constants";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  DialogContentText,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    position: "absolute",
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function RestaurantInfo() {
  const classes = useStyles();
  const [index, setIndex] = useState(0);
  const { restaurantContext } = useContext(RestaurantContext);
  const [restaurant, setRestaurant] = restaurantContext;
  const [loading, setLoading] = useState(false);
  const [comments, setComments] = useState([]);
  useEffect(() => {
    getComments();
  }, []);

  const getComments = () => {
    setLoading(true);
    getComment(restaurant._id)
      .then((res) => {
        setComments(res.data.data.comment);
        console.log(res.data.data.comment);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });
  };
  const handleClick = (newIndex) => {
    setIndex(newIndex);
    console.log(index);
  };

  const handleClose = () => {
    setRestaurant(null);
  };
  return (
    <div style={{ width: "90vh" }}>
      {loading ? (
        <PageLoading />
      ) : (
        restaurant && (
          <Dialog
            open={restaurant}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
            style={{ width: "100%", height: "100vh" }}
          >
            <DialogTitle
              id="form-dialog-title"
              style={{ alignItems: "center", textAlign: "center" }}
            >
              {restaurant.name}
            </DialogTitle>
            <DialogContent
              style={{
                display: "flex",
                flexDirection: "column",
                width: "100%",
                height: "80vh",
              }}
            >
              <div>
                <div>
                  <div style={{ marginLeft: "2vh" }}>
                    {restaurantInfoTabs &&
                      restaurantInfoTabs.map((type) => (
                        <Tag
                          bgColor={index === type.index ? "#FFA500" : "#C4C4C4"}
                          textColor={index === type.index ? "#FFF" : "#000"}
                          text={type.value}
                          onClick={() => handleClick(type.index)}
                        />
                      ))}
                  </div>
                </div>
                <div>
                  <div>{index === 0 && <Info />}</div>
                  <div>{index === 1 && <Review comments={comments} />}</div>
                  <div x>{index === 2 && <Maps />}</div>
                </div>
              </div>
            </DialogContent>
          </Dialog>
        )
      )}
    </div>
  );
}
