import React, { useContext } from "react";
import { RestaurantContext } from "../../../context/RestaurantContext";
import { Row, Col } from "react-bootstrap";
import Slider from "react-slick";

export default function Info() {
  const { restaurantContext } = useContext(RestaurantContext);
  const [restaurant, setRestaurant] = restaurantContext;
  let settings = {
    dots: true,
  };
  return (
    <div style={{ width: "70vh", height: "70vh" }}>
      {restaurant && (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "flex-start",
          }}
        >
          <div>
            <h4>Address</h4>
            <div>{restaurant.address}</div>
          </div>
          <div>
            <h4>Pictures</h4>
            <Slider {...settings}>
              {restaurant.photos.length !== 0 ? (
                restaurant.photos.map((photo) => {
                  return (
                    <div>
                      <img
                        src={photo}
                        alt={restaurant.name}
                        style={{
                          maxHeight: "50vh",
                          maxWidth: "70vh",
                          alignItems: "center",
                          textAlign: "center",
                        }}
                      ></img>
                    </div>
                  );
                })
              ) : (
                <div>
                  <img
                    src={"https://i.stack.imgur.com/y9DpT.jpg"}
                    alt={restaurant.name}
                    style={{
                      maxHeight: "100%",
                      margin: "auto",
                      width: "auto",
                    }}
                  ></img>
                </div>
              )}
            </Slider>
          </div>
        </div>
      )}
    </div>
  );
}
