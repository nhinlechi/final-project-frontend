import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Fragment } from "react";
import Button from "@material-ui/core/Button";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import classes from "./HistoryMeetingDialog.module.css";
import startIcon from "../../assets/icons/star.svg";
export default function HistoryMeetingDialog(props) {
  const theme = useTheme();

  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const listMeetingHistory = props.listHistoryMeeting;

  const handleConfirm = (status) => {};

  const handleDelete = () => {};

  const handleClose = () => {
    props.setIsOpen(false);
  };

  const statusMeeting = (status) => {
    switch (status) {
      case 0:
        return "Đang đợi gặp mặt";
      case 1:
        return "Đã gặp mặt";
      case 2:
        return "Đã huỷ";

      default:
        break;
    }
  };

  const meetingHistoryListJSX = listMeetingHistory.map((item) => {
    const dateTime = new Date(+item.datetime);
    return (
      <div className={classes.wrapper}>
        <div className={classes.content}>
          <div className={classes.title}>
            <div className={classes.name}>{item.placeid.name}</div>
            <div className={classes.rating}>
              <div>{item.placeid.rating}</div>
              <img src={startIcon} />
            </div>
          </div>
          <div
            className={classes.address}
          >{`Địa chỉ: ${item.placeid.address}`}</div>
          <div>{`Vào ngày: ${dateTime.toUTCString()}`}</div>
          <div>{`Trạng thái: ${statusMeeting(item.status)}`}</div>
        </div>
      </div>
    );
  });

  return (
    <Fragment>
      <Dialog
        fullScreen={fullScreen}
        open={props.open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="responsive-dialog-title">Meeting History</DialogTitle>
        <DialogContent>
          <DialogContentText>{meetingHistoryListJSX}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={handleClose} color="primary">
            Cancel
          </Button>

          <Button color="btn_ffa500" onClick={handleDelete}>
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </Fragment>
  );
}
