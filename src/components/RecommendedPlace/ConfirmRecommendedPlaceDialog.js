import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import { Fragment } from "react";
import Button from "@material-ui/core/Button";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import classes from "./RecommendedPlace.module.css";
import startIcon from "../../assets/icons/star.svg";
import {confirmPlace, deletePlace, sendPlace} from "../../api/matchApi"
export default function ConfirmRequestPlaceDialog(props) {

  const theme = useTheme();

  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const notificationId = props.infor.notificationid;
  const isMine = localStorage.getItem("userid") === props.infor.sender;
  let place = {};
  if (props.infor.place) place = props.infor.place;
  const date = props.infor.date;
  const dateTime = new Date(+date);

  const handleConfirm = (status) => {
    confirmPlace({
        status: status,
        notificationid: notificationId,
    }).then((res)=>{
        handleClose();
        props.setNoti(false);
    }).catch((error)=>{
        console.log("Confirm request place", error);
    })
  };

  const handleDelete = () =>{
      deletePlace({notificationid:notificationId}).
      then(res=>{
          handleClose();
          props.setNoti(false);

      }).catch(err=>console.log(err));
  }

  const handleClose = () => {
    props.setIsOpen(false);
  };
  return (
    <Fragment>
      <Dialog
        fullScreen={fullScreen}
        open={props.open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="responsive-dialog-title">
          {isMine
            ? "Waiting for your friend reply!"
            : "Your friend is sending request place, please confirm!"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <div className={classes.wrapper}>
              <div className={classes.content}>
                <div className={classes.title}>
                  <div className={classes.name}>{place.name}</div>
                  <div className={classes.rating}>
                    <div>{place.rating}</div>
                    <img src={startIcon} />
                  </div>
                </div>
                <div className={classes.address}>{place.address}</div>
                <div>{`Vào ngày: ${dateTime.toUTCString()}` }</div>
              </div>
            </div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={isMine?handleClose:()=>{handleConfirm(false)}} color="primary">
            Cancel
          </Button>
          {!isMine ? (
            <Button color="btn_ffa500" onClick={()=>{
                handleConfirm(true);
            }}>
              Confirm
            </Button>
          ) : (
            <Button color="btn_ffa500" onClick={handleDelete}>
              Delete
            </Button>
          )}
        </DialogActions>
      </Dialog>
    </Fragment>
  );
}
