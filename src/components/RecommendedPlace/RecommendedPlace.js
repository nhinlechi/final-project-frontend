import React from "react";
import classes from "./RecommendedPlace.module.css";
import startIcon from "../../assets/icons/star.svg"
const RecommendedPlace = (props) => {
  const place = props.place;
  const isChoosed = props.currentPlace === props.place._id;
  const styleSelected = isChoosed?{backgroundColor:"aquamarine"}:{}
  const selectedPlaceHandler = () =>{
    props.setSelectedPlace(props.place._id);
  }
  return (
    <div className={classes.wrapper} style={styleSelected} onClick={selectedPlaceHandler}>
      <div className={classes.content}>
          <div className={classes.title}>
              <div className={classes.name}>
                  {place.name}
              </div>
              <div className={classes.rating}>
                  <div>{place.rating}</div>
                  <img src={startIcon}/>
              </div>
          </div>
          <div className={classes.address}>{place.address}</div>
      </div>    
    </div>
  );
};

export default RecommendedPlace;
