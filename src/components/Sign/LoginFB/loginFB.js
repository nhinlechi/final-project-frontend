import React from 'react';
import { useHistory } from 'react-router-dom';
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import { loginFacebook } from '../../../api/authApi';

export default function LoginFB(props) {
  let history = useHistory();

  const responseFacebook = (response) => {
    console.log("login fb success", response);
    let data = {
      accesstoken: response.accessToken
    };

    loginFacebook(data)
      .then((res) => {
        console.log("res token", res)
        if (res.status === 200) {
          localStorage.setItem("token", res.data.data.token);
          localStorage.setItem("userid", res.data.data.userid);
          localStorage.setItem("typeSignin", "facebook"); // type sign in
          history.push("/");
        }
      })
      .catch((err) => {
        console.log("err token", err)
      });
  }

  return (

    <FacebookLogin
      appId={process.env.REACT_APP_FACEBOOK_APP_ID}
      autoLoad={false}
      fields="name,email,picture"
      callback={responseFacebook}
      render={renderProps => (
        <button onClick={renderProps.onClick} disabled={renderProps.disabled} className="login50-facebook">
          Login Facebook
        </button>
      )}
    />
  )
}
