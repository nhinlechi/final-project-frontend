import { useEffect, useState } from "react";
import markerIcon from "../../assets/icons/maps-and-flags.svg";
import restaurantIcon from "../../assets/icons/restaurant.svg";
import barIcon from "../../assets/icons/bar.svg";
import coffeeIcon from "../../assets/icons/coffee.svg";
import shoppingIcon from "../../assets/icons/mall.svg";
import { recomposeColor } from "@material-ui/core";

export default function Marker(props) {
  const type = props.type;
  const [src, setSrc] = useState(markerIcon);

  useEffect(() => {
    switch (type) {
      case "restaurant":
        setSrc(restaurantIcon);
        break;
      case "cafe":
        setSrc(coffeeIcon);
        break;
      case "bar":
        setSrc(barIcon);
        break;
      case "shopping_mall":
        setSrc(shoppingIcon);
        break;

      default:
        break;
    }
  }, [type]);

  return (
    <img
      onClick={props.onClick}
      title={props.title}
      src={src}
      style={{
        width: "32px",
        height: "32px",
        position: "relative",
        bottom: "-16px",
      }}
    />
  );
}
