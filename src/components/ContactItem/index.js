import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function ContactItem(props) {
    const userId = localStorage.getItem("userid")
    const matchInfor = props.matchInfor;
    const matchId = matchInfor._id;
    const participant = matchInfor.participants.find((item)=>item._id!==userId);
    const style = matchId===props.isActive?{backgroundColor:"#F8D088"}:{};

  return (
    <div className="msg-item" onClick={props.onClick} style={style}>
        <div className="item" style={{paddingLeft:"7px"}}>
            {/* avater */}
            <div style={{position: 'relative'}}>
                <div className="avatar-msg avatar-msg--m ">
                    <img className="avatar-img outline" alt=""
                    src={participant.avatar[0]} />
                </div>
            </div>
            {/* item content */}
            <div className="item-content-container">
                <div className="item-title d-flex align-items-center ">
                    <div className="item-title-name">
                        <span>{`${participant.firstname} ${participant.lastname}`}</span>
                    </div>
                </div>
                <div className="d-flex align-items-center"> 
                    <div className="item-message truncate">
                        <div className="truncate">
                            <div style={{ lineHeight: "20px" }}>
                            <span>{props.lastMsg.type==="text"?props.lastMsg.message:"Đã gửi một ảnh"}</span>
                            </div>
                        </div>
                    </div>
                    <div className="item-action">
                        <div className="item-action__menu ">
                            <i className="fa fa-tab-icon-more func-setting__icon"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  );
}
