import React, { useEffect, useState, useContext, forwardRef } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import { useTheme } from "@material-ui/core/styles";
import { LoadingContext } from "../../context/LoadingContext";
import { getRecommendPlace, sendPlace } from "../../api/matchApi";
import RecommendedPlace from "../RecommendedPlace/RecommendedPlace";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
} from "@material-ui/core";
import { getSearchPlace } from "../../api/placeApi";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const ROW_PER_PAGE = 20;

const CustomInput = forwardRef(({ value, onClick }, ref) => (
    <button style={{backgroundColor:"#FFA500", borderRadius:"10px", padding:"5px 10px"}} onClick={onClick} ref={ref}>
      {value}
    </button>
  ));
  
export default function RecommendedPlaceDialog(props) {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down("sm"));
  const [selectedPlace, setSelectedPlace] = useState("");
  const [listRecommendedPlaces, setListRecommendedPlaces] = useState([]);
  const [selectedDate, setSelectedDate] = useState(new Date());
  const loadingContext = useContext(LoadingContext);
  const matchId = props.matchId;
  const [value, setValue] = useState("recommend");
  const [isRecommendedPlace, setIsRecommendedPlace] = useState(true);
  const [query, setQuery] = useState("");
  const [pageNumber, setPageNumber] = useState(1);
  const [errorText, setErrorText] = useState("");

  const getListPlaceHandler = (query, type, pageNumber) => {
    getSearchPlace(query, type, pageNumber, ROW_PER_PAGE)
      .then((res) => {
        console.log(res);
        setListRecommendedPlaces((prevPlaces) => {
          let result = [...prevPlaces];
          res.data.data.places.forEach(element1 => {
            let isSame = false;
            prevPlaces.forEach(element2=>{
              if(element1._id===element2._id)
                isSame = true;
            })
          if (!isSame)
            result.push(element1);
          });
        console.log(result);
          return result;
        });
        setIsRecommendedPlace(listRecommendedPlaces.length>=res.data.data.total);
      })
      .catch((error) => console.log(error));
  };

  const getRecommendPlaceHandler = () => {
    loadingContext.setIsLoading(true);
    getRecommendPlace({ id: matchId })
      .then((res) => {
        if (res.status === 200) {
          setListRecommendedPlaces(res.data.data);
          setIsRecommendedPlace(true);
        } else {
        }
      })
      .catch((error) => {
        console.error("Get list recommended place in chat", error);
      });
    loadingContext.setIsLoading(false);
  };

  useEffect(() => {
    getRecommendPlaceHandler();
  }, []);

  const listRecommendedPlace = listRecommendedPlaces.map((place) => {
    return (
      <RecommendedPlace
        place={place}
        key={place._id}
        setSelectedPlace={setSelectedPlace}
        currentPlace={selectedPlace}
      />
    );
  });

  const handleClose = () => {
    props.setIsOpen(false);
  };

  const sendPlaceHandler = () => {
    const currentDate = new Date();
    const timeChoose = selectedDate.getTime();
    if(selectedPlace==="" ){
        setErrorText("Vui lòng chọn nhà hàng ");
        return;
    }
    if((timeChoose-currentDate.getTime()<(1000*60*30))){
        setErrorText("Vui lòng chọn thời gian muộn hơn hiện tại ít nhất 30 phút");
        return;
    }
    setErrorText("");
    handleClose();
    sendPlace({
      matchid: matchId,
      placeid: selectedPlace,
      date: selectedDate.getTime(),
    });
  };
  const handleChange = (e) => {
    setIsRecommendedPlace(true);
    setListRecommendedPlaces([]);
    if (e.target.value === "recommend") getRecommendPlaceHandler();
    else {
      getListPlaceHandler(query, e.target.value, 1);
    }
    setPageNumber(1);
    setValue(e.target.value);
  };

  const loadMorePlace = (e) => {
    getListPlaceHandler(query, value, pageNumber + 1);
    setPageNumber(pageNumber+1);
  };

  const handleSearch = (e) => {
    setQuery(e.target.value);
  };

  useEffect(() => {
    let timer = setTimeout(() => {
    setListRecommendedPlaces([]);
    setIsRecommendedPlace(true);

      if (value === "recommend" && query.trim().length!==0) getListPlaceHandler(query, "", 1);
      else if(query.trim().length!==0) {
        setListRecommendedPlaces([]);
        getListPlaceHandler(query, value, 1);
      }

      setPageNumber(1);
    }, 500);
    return () => {
      clearTimeout(timer);
    };
  }, [query]);
  return (
    <div>
      <Dialog
        fullScreen={fullScreen}
        open={props.open}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
        aria-labelledby="scroll-dialog-title"
        aria-describedby="scroll-dialog-description"
      >
        <DialogTitle id="responsive-dialog-title">
          {"Lựa chọn địa điểm gặp mặt"}
          <div className="event-search-bar" style={{ margin: "10px 0px" }}>
            <FontAwesomeIcon
              icon="search"
              size="1x"
              color="#858585"
              className="ml-3"
            />
            <input
              style={{ marginLeft: "1vh", marginTop: "1vh" }}
              type="text"
              className="search-places-input"
              placeholder="Enter search"
              value={query}
              onChange={handleSearch}
            />
          </div>

          <FormControl component="fieldset">
            <FormLabel component="legend">Filter</FormLabel>
            <RadioGroup
              aria-label="gender"
              name="gender1"
              value={value}
              onChange={handleChange}
              style={{ display: "flex", flexDirection: "row" }}
            >
              <FormControlLabel
                value="recommend"
                control={<Radio />}
                label="Recommend"
              />
              <FormControlLabel
                value="restaurant"
                control={<Radio />}
                label="Restaurant"
              />
              <FormControlLabel value="cafe" control={<Radio />} label="Cafe" />
              <FormControlLabel value="bar" control={<Radio />} label="Bar" />
              <FormControlLabel
                value="shopping_mall"
                control={<Radio />}
                label="Shopping"
              />
            </RadioGroup>
          </FormControl>
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            {listRecommendedPlace}
            {isRecommendedPlace === false && (
              <div
                style={{
                  display: "flex",
                  justifyItems: "center",
                  flexDirection: "column",
                }}
              >
                <button
                  style={{
                    border: "1px solid #FFCD72",
                    borderRadius: "10px",
                    marginBottom: "10px",
                  }}
                  onClick={loadMorePlace}
                >
                  Load more
                </button>
              </div>
            )}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
        <DatePicker
              dateFormat="Pp"
              selected={selectedDate}
              showTimeSelect
              onChange={(date) => {
                setSelectedDate(date);
              }}
              onSelect={(date) => {
                setSelectedDate(date);
              }}
              customInput={<CustomInput/>}/>
              <div style={{width:"100%", color:"red"}}>
                  {errorText}
              </div>
            

          <Button autoFocus onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={sendPlaceHandler} color="btn_ffa500" autoFocus>
            Choose
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
