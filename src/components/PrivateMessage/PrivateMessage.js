import React, { useEffect, useState, useContext } from "react";
import { Redirect } from "react-router";
import useChat from "./useChat";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useParams } from "react-router-dom";
// component
import MessageItem from "../../components/MessageItem/index";
import { getChat, getMeetingHistory, getRecommendPlace, sendPlace } from "../../api/matchApi";
import { LoadingContext } from "../../context/LoadingContext";
import RecommendedPlaceDialog from "./RecommendedPlaceDialog";
import firebase from "firebase/app";
import "firebase/messaging";
import ConfirmRequestPlaceDialog from "../RecommendedPlace/ConfirmRecommendedPlaceDialog";
import classes from "./PrivateMessage.module.css";
import sendIcon from "../../assets/icons/send-message.svg";
import placeIcon from "../../assets/icons/place.svg";
import HistoryMeetingDialog from "../HistoryMeeting/HistoryMeetingDialog";

const messaging = firebase.messaging();

const PrivateMessage = (props) => {
  const loadingContext = useContext(LoadingContext);
  const params = useParams();
  const messagesEndRef = React.createRef();
  const matchId = params.matchId;
  const [userChatInform, setUserChatInform] = useState({
    firstname: "",
    lastname: "",
    avatar: [],
  });

  const [isOpenModal, setIsOpenModal] = useState(false);

  const [listMessage, setListMessage] = useState([]);

  /**
   * Custom hook, handle send chat and receive new message
   */
  const { messages, sendMessage, setListMessages } = useChat(
    matchId,
    listMessage
  );
  const [currentMessage, setCurrentMessage] = useState("");

  const [lastMessage, setLastMessage] = useState({ index: 0 });
  const [isLoadMore, setIsLoadMore] = useState(false);
  const getChatHandler = () => {
    getChat({ matchId: matchId })
      .then((res) => {
        if (res.status === 200) {
          const listMsg = res.data.data.sort((a, b) => a.created - b.created);
          if (listMsg) setLastMessage(listMsg[0]);
          setListMessage(listMsg);
          loadingContext.setIsLoading(false);
        } else {
          console.log(res);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const loadMoreHandler = () => {
    loadingContext.setIsLoading(true);
    getChat({
      matchId: matchId,
      lastMessage: lastMessage ? lastMessage.index : lastMessage,
    })
      .then((res) => {
        if (res.status === 200) {
          const listMsg = res.data.data.sort((a, b) => a.created - b.created);
          if (listMsg) setLastMessage(listMsg[0]);
          setIsLoadMore(true);
          setListMessages(listMsg);
          loadingContext.setIsLoading(false);
        } else {
          console.log(res);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const [isCallUser, setIsCallUser] = useState(false);
  /**
   * Get list message
   */
  useEffect(() => {
    props.setActive(matchId);

    if (props.listMatch.length !== 0) {
      const matchItem = props.listMatch.find(
        (element) => element._id === matchId
      );
      const userid = localStorage.getItem("userid");
      const userInform = matchItem.participants.find(
        (element) => element._id !== userid
      );
      loadingContext.setIsLoading(true);
      setUserChatInform(userInform);
      getChatHandler();
    }
  }, [props.listMatch, matchId]);

  /**
   * JSX list message
   */
  const messageHandler = messages.map((element) => {
    const message = element.data;
    const userid = localStorage.getItem("userid");
    const isMine = element.sender === userid;
    let time = new Date(element.created);
    return (
      <MessageItem
        key={element.created}
        text={message}
        title={time.toString()}
        sender={isMine ? "mine" : ""}
      />
    );
  });

  /**
   * Auto scrool when having new message
   */
  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };
  useEffect(() => {
    if (!isLoadMore) {
      scrollToBottom();
    }
  }, [messages]);

  /**
   * Send chat handler
   * @param {Event} e
   */
  const sendChatClickHandler = (e) => {
    e.preventDefault();
    if (currentMessage.trim().length !== 0) {
      setIsLoadMore(false);
      sendMessage(currentMessage);
      setCurrentMessage("");
    }
  };

  const [requestPlaceNotification, setRequestPlaceNotification] = useState({});
  const [isPlaceNotification, setIsPlaceNotification] = useState(false);
  const [openDialogConfirm, setIsOpenDialogConfirm] = useState(false);
  const [meetingHistory, setMeetingHistory] = useState([]);
  const [openHistoryMeetingDialog, setOpenHistoryMeetingDialog] = useState(false);

  /**
   * Listening the notification for match now
   */
  messaging.onMessage((payload) => {
    console.log("Message received. ", payload);

    switch (payload.notification.body) {
      case "8":
        setIsPlaceNotification(true);
        const data = JSON.parse(payload.data.stringdata);
        setRequestPlaceNotification(data);
    }
  });

  useEffect(() => {
    setIsOpenDialogConfirm(false);
  }, []);

  const callUser = () => {
    console.log("user", localStorage.getItem("userid"));
    setIsCallUser(true);
  };

  const getMeetingHistoryHandler = () => {
      getMeetingHistory({matchid:matchId}).then(
        (res)=>{
          setMeetingHistory(res.data.data);
          setOpenHistoryMeetingDialog(true);
        }
      )
      .catch(err=>console.error(err));
  }


  return (
    <React.Fragment>
      {isCallUser && (
        <Redirect to={"/video-call?type=caller&id=" + userChatInform._id} />
      )}
      <div className={`content scrollable ${classes.private}`}>
        <div className="toolbar-msg d-flex">
          <div className="d-flex flx-1">
            <div style={{ position: "relative" }}>
              <div className="avatar-msg avatar-msg--m ">
                <img
                  className="avatar-img outline"
                  alt=""
                  src={userChatInform.avatar[0]}
                />
              </div>
            </div>
            <div className="msg-header-title">
              <div className="d-flex align-items-center main-title-container">
                <div className="title header-title d-flex align-items-center">{`${userChatInform.firstname} ${userChatInform.lastname}`}</div>
              </div>
            </div>
          </div>
          <div className="d-flex align-items-center">
            {isPlaceNotification && (
              <div
                className="icon-outline-video mr-3"
                onClick={() => {
                  setIsOpenDialogConfirm(true);
                }}
              >
                <FontAwesomeIcon
                  icon="location-arrow"
                  size="2x"
                  color="#FFA500"
                  className="animationplace"
                />
              </div>
            )}
            <div className="icon-outline-cog mr-3" onClick={getMeetingHistoryHandler}>
              <FontAwesomeIcon icon="history" size="2x" color="#FFA500" />
            </div>
            <div className="icon-outline-video mr-3">
              <FontAwesomeIcon
                icon="video"
                size="2x"
                color="#FFA500"
                onClick={callUser}
              />
            </div>
            <div className="icon-outline-cog mr-3">
              <FontAwesomeIcon icon="cog" size="2x" color="#FFA500" />
            </div>
          </div>
        </div>

        {/* Content */}
        <div className="chat-view">
          {!lastMessage ||
            (lastMessage.index !== 0 && (
              <div
                style={{
                  display: "flex",
                  justifyItems: "center",
                  flexDirection: "column",
                }}
              >
                <button
                  style={{
                    border: "1px solid #FFCD72",
                    borderRadius: "10px",
                    marginBottom: "10px",
                  }}
                  onClick={loadMoreHandler}
                >
                  Load more
                </button>
              </div>
            ))}
          {messageHandler}
          <div ref={messagesEndRef} />
        </div>
        <form onSubmit={sendChatClickHandler}>
          <div className="compose">
            <div>
              <button
                onClick={(e) => {
                  e.preventDefault();
                  setIsOpenModal(true);
                }}
                title="Gửi địa điểm hẹn gặp"
                type="button"
              >
                <img
                  src={placeIcon}
                  style={{ width: "32px", height: "32px" }}
                />
                
              </button>
            </div>
            <input
              onChange={(event) => {
                setCurrentMessage(event.target.value);
              }}
              value={currentMessage}
              type="text"
              className="compose-input"
              placeholder="Type a message, @name"
              style={{ marginLeft: "1rem" }}
            />
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                width: "20%",
              }}
            >
              <button
                onClick={sendChatClickHandler}
                type="submit"
              >
                <img src={sendIcon} style={{ width: "32px", height: "32px" }} />
              </button>
            </div>
          </div>
        </form>
      </div>
      <RecommendedPlaceDialog
        open={isOpenModal}
        setIsOpen={setIsOpenModal}
        matchId={matchId}
      />
      <ConfirmRequestPlaceDialog
        open={openDialogConfirm}
        setIsOpen={setIsOpenDialogConfirm}
        infor={requestPlaceNotification}
        setNoti={setIsPlaceNotification}
      />
      <HistoryMeetingDialog
        open={openHistoryMeetingDialog}
        listHistoryMeeting = {meetingHistory}
        setIsOpen = {setOpenHistoryMeetingDialog}
      />

    </React.Fragment>
  );
};

export default PrivateMessage;
