import React, { useEffect, useState } from "react";
import Avatar from "@material-ui/core/Avatar";
import MatchContext from "../../context/MatchContext";
import {useContext} from 'react'
import {getUserById} from '../../api/userApi'
import {getGroupById} from '../../api/groupApi'
 
export default function NotificationItem(props) {
  const notificationData = props.notificationItem;  
  const matchContext = useContext(MatchContext);
  const [userInfor, setUserInfor] = useState({firstname:"",lastname:"",avatar:[]});
  const [groupInfor, setGroupInfor] = useState({name:"",photos:[]});

  const checkUser = (user1, user2) => {
    return user1._id === localStorage.getItem("userid") ? user2 : user1;
  };
  const notificationItem = () => {
      let anotherUser;
    switch (notificationData.code) {
      case 1:
         anotherUser = checkUser(
          notificationData.data.user1,
          notificationData.data.user2
        );
        const timeOutNow = new Date(+notificationData.data.timeout);

        return {
            title:  `Bạn có một request match now với ${anotherUser.firstname} ${anotherUser.lastname}`,
            content: `Hết hạn vào ${timeOutNow.toUTCString()}`,
            action: () => {
            const dateNow = new Date();
            if(dateNow.getTime()>=notificationData.data.timeout)
                return;
            console.log(notificationData.data.timeout);
            matchContext.setMatchInfor({
                fullname: `${anotherUser.lastname} ${anotherUser.firstname}`,
                avatar: anotherUser.avatar[0],
                notificationid: notificationData._id,
                timeout: notificationData.data.timeout,
              });
              matchContext.setIsProcessMatching(true);
              props.closeNotify();
          },
          avatar: anotherUser.avatar[0],
        };
      case 2:
         anotherUser = checkUser(
          notificationData.data.user1,
          notificationData.data.user2
        );
        const timeOut = new Date(+notificationData.data.timeout);
        return {
          title:  `Bạn có một request match later với ${anotherUser.firstname} ${anotherUser.lastname}`,
          content: `Hết hạn vào ${timeOut.toUTCString()}`,
          action: () => {
            const dateNow = new Date();
            if(dateNow.getTime()>=notificationData.data.timeout)
                return;
            console.log(notificationData.data.timeout);
            matchContext.setMatchInfor({
                fullname: `${anotherUser.lastname} ${anotherUser.firstname}`,
                avatar: anotherUser.avatar[0],
                notificationid: notificationData._id,
                timeout: notificationData.data.timeout,
              });
              matchContext.setIsProcessMatching(true);
              props.closeNotify();
          },
          avatar: anotherUser.avatar[0],
        };
        case 4:

          return {
            title: `Bạn có một yêu cầu tham gia nhóm từ ${userInfor.firstname} ${userInfor.lastname}`,
            content: `Nhóm ${groupInfor.name}`,
            action: () => {props.setOpenConfirm(true)},
            avatar: groupInfor.avatar,
          }
        case 8:
            const place = notificationData.data.place;
            return {
                title: `Bạn có một đề nghị hẹn gặp tại ${place.name}`,
                content: place.address,
                action: () => {},
                avatar: place.photos[0]
            }
    }
    return {
      title: "Bạn có một request match now",
      content: "test",
      action: () => {},
      avatar: "test",
    };
  };
  useEffect(
    ()=>{
      if(notificationData.code===4){
        props.setConfirmRequestJoinGroup({serviceid: notificationData.serviceid, userid: notificationData.data.userid, notificationid: notificationData._id})

        getUserById(notificationData.data.userid).then(
          (res)=>{
            setUserInfor(res.data.data);
          }
        )
        getGroupById(notificationData.serviceid).then(res=>{
          setGroupInfor(res.data.data);
        })
      }
    },[]
  )
  const notification = notificationItem();
  return (
    <div className="row mt-2" onClick={notification.action}>
      <div className="col-2 d-flex align-items-center">
        <Avatar alt="Test" src={notification.avatar} />
      </div>
      <div className="col-10 d-flex flex-column">
        <div className="text_20_b">{notification.title}</div>
        <div>{notification.content}</div>
      </div>
    </div>
  );
}
