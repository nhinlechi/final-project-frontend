import React, { useContext, useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Row } from "react-bootstrap";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import firebase from "firebase/app";
import "firebase/messaging";
import { useHistory } from "react-router-dom";
// component
import LoadingSVG from "../../assets/images/Rolling-1s-35px.svg";
// api
import {
  matchNow,
  cancelRequestMatch,
  requestMatchLater,
} from "../../api/matchApi";
import LocationContext from "../../context/LocationContext";
import MatchContext from "../../context/MatchContext";
import ProfileContext from "../../context/ProfileContext";


const TIME_REQUEST = 30000;
// Hàm gửi yêu cầu ghép đôi
// Hàm huỷ yêu cầu

export default function RequestMatch(props) {
  const [isRequestMatch, setIsRequestMatch] = useState(false);
  const locationContext = useContext(LocationContext);
  const matchContext = useContext(MatchContext);
  let history = useHistory();
  const profileContext = useContext(ProfileContext);
  const dataProfile = profileContext.dataProfile;
  console.log(dataProfile);
  const [error, setError] = useState("");

  const messaging = firebase.messaging();

  const receiveNotificationFoundMatch = (payload) => {
    const data = JSON.parse(payload.data.stringdata);
    const userMatched = data.user1._id===localStorage.getItem("userid")?data.user2:data.user1;
    matchContext.setMatchInfor({
      fullname: `${userMatched.lastname} ${userMatched.firstname}`,
      avatar: userMatched.avatar[0],
      notificationid: data.notificationid,
      timeout: data.timeout,
    });
    matchContext.setIsProcessMatching(true);

    setIsRequestMatch(false);
  };

  const receiveNotificationMatched = (payload) => {
    setIsRequestMatch(false);
    const data = JSON.parse(payload.data.stringdata);
    const matchId = data.matchId;
    if (matchId !== "") history.push(`/messages/${matchId}`);
  };

  /**
   * Listening the notification for match now
   */
  messaging.onMessage((payload) => {
    console.log("Message received. ", payload);
    // if body equal 1, web will pop up modal

    switch (payload.notification.body) {
      case "2":
      case "1":
        receiveNotificationFoundMatch(payload);
        break;
      case "3":
        receiveNotificationMatched(payload);
        break;
    }
  });

  const checkProfile = ()=>{
    if(dataProfile.gender===0 || dataProfile.relationship===0){
      setError("Vui lòng cập nhật giới tính và mối quan hệ")
      return false;
    }
    return true;
  }

  /**
   * Request match now handler
   */
  const requestMatchHandler = async () => {
    let dataMatchNowBody = props.matchNowBody;
    dataMatchNowBody.lat = locationContext.location.latitude;
    dataMatchNowBody.long = locationContext.location.longitude;
    setError("");
    if(!checkProfile()){
      return;
    }
    if (!props.isMatchLater)
      matchNow(dataMatchNowBody)
        .then((response) => {
          if (response.status === 200) {
            // Đã nhận yêu cầu
            setIsRequestMatch(true);
          } else {
            setIsRequestMatch(false);
          }
        })
        .catch((error) => {
          console.error("Request match now", error, error.message);
          setIsRequestMatch(false);
          setError("Bạn đang trong quá trình matching, vui lòng kiểm tra thông báo!")
        });
    else
      requestMatchLater(dataMatchNowBody)
        .then((res) => {
          if (res.status === 200) setIsRequestMatch(true);
          else setIsRequestMatch(false);
        })
        .catch((error) => {
          console.error("Request match later", error, error.message);
          setIsRequestMatch(false);
          setError("Bạn đang trong quá trình matching, vui lòng kiểm tra thông báo!")

        });
  };

  /**
   * Cancel request match (now and later) handler
   */
  const cancelRequestMatchHandler = async () => {
    setIsRequestMatch(false);
    await cancelRequestMatch()
      .then((response) => {
        if (response.status === 200) {
          // Đã nhận yêu cầu
          setIsRequestMatch(false);
        } else {
          setIsRequestMatch(true);
        }
      })
      .catch((error) => {
        console.error("Cancel request match", error, error.message);
        setIsRequestMatch(true);
      });
  };

  

  return (
    <React.Fragment>
      {/* <div className="content-container"> */}
      <Row className="mt-1">
        <div className="find-object">
          <div className="btn-function__size70">
            <button
              type="button"
              onClick={
                !isRequestMatch
                  ? requestMatchHandler
                  : cancelRequestMatchHandler
              }
            >
              {isRequestMatch === false ? (
                <FontAwesomeIcon
                  icon="search"
                  size="2x"
                  color="#FFF"
                  style={{ marginTop: "20px" }}
                />
              ) : (
                <img src={LoadingSVG} style={{ marginTop: "17px" }} alt="" />
              )}
            </button>
          </div>
        </div>
      </Row>
      {/* <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <ToastContainer /> */}
      <div style={{textAlign:"center", color:"red", marginTop:"1rem"}}>{error}</div>
    </React.Fragment>
  );
}
