import { useState } from "react";
import defaultImage from "../../../assets/images/default-img-place.jpg"

export default function Image(props){
    const [src,  setSrc] = useState(props.src);

    const errorHandler = () =>{
        setSrc(defaultImage);
    }

    return <img onError={errorHandler} src={src} alt = {props.alt} onClick={props.onClick} className={props.className} style={props.style}/>
}