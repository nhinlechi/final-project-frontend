import Marker from "../Marker/Marker";
import GoogleMapReact from "google-map-react";
import React, { useContext, useEffect, useState } from "react";
import LocationContext from "../../context/LocationContext";
import { getPlaceInViewport } from "../../api/placeApi";
import { brown } from "@material-ui/core/colors";
import DefaulMarker from "react-google-maps/lib/components/Marker";
import { RestaurantContext } from "../../context/RestaurantContext";

export default function Map(props) {
  const locationContext = useContext(LocationContext);
  const [bounds, setBound] = useState({
    lat_left_top: 0,
    long_left_top: 0,
    lat_right_bottom: 0,
    long_right_bottom: 0,
  });

  const [placeLocations, setPlaceLocations] = useState([]);
  const defaultMap = {
    center: {
      lat: locationContext.location.latitude,
      lng: locationContext.location.longitude,
    },
    zoom: 15,
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      getPlaceInViewport(bounds)
        .then((res) => {
          setPlaceLocations(res.data.data.places);
        })
        .catch((err) => console.log(err));
    }, 1000);
    return () => clearTimeout(timer);
  }, [bounds]);

  const { restaurantContext } = useContext(RestaurantContext);
  const [restaurant, setRestaurant] = restaurantContext;
  const clickRestaurantMarkerHanlder = (place) => {
    setRestaurant(place);
  };

  const listLocationMarkerJSX = placeLocations.map((place) => (
    <Marker
      onClick={() => clickRestaurantMarkerHanlder(place)}
      title={`${place.name} \n ${place.address}`}
      key={place._id}
      lat={place.location.lat}
      lng={place.location.lng}
      type={place.type}
    />
  ));

  const onChangeMapHandler = ({ bounds }) => {
    const { nw, se } = bounds;
    setBound({
      lat_left_top: nw.lat,
      long_left_top: nw.lng,
      lat_right_bottom: se.lat,
      long_right_bottom: se.lng,
    });
  };
  console.log("API key", process.env.REACT_APP_GOOGLE_API_KEY);

  return (
    <div style={{ height: "100vh", width: "100%" }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: process.env.REACT_APP_GOOGLE_API_KEY }}
        center={defaultMap.center}
        defaultZoom={defaultMap.zoom}
        onChange={onChangeMapHandler}
      >
        {listLocationMarkerJSX}
        <Marker lat={defaultMap.center.lat} lng={defaultMap.center.lng} />
      </GoogleMapReact>
    </div>
  );
}
