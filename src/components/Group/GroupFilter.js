import React, { useEffect, useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import { Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { GroupContext } from "../../context/GroupContext";
import Chip from "../Chip/index";
import classes from "./GroupFilter.module.css";
// api
import { getGroupOfUser, getListGroup } from "../../api/groupApi";

export default function GroupFilter(props) {
  const history = useHistory();
  const { listGrp, txtSearch, totalGroup } = useContext(GroupContext);
  const [listGroup, setListGroup] = listGrp;
  const [search, setSearch] = txtSearch;
  const [total, setTotal] = totalGroup;

  const [listMyGroup, setListMyGroup] = useState([]);
  const [currentType, setCurrentType] = useState("");

  const pressBtnChip = (newValue, value, setValue) => {
    if (newValue._id === value) {
      setValue(null);
    } else {
      setValue(newValue);
      history.push(`/groups/detail/${newValue._id}`);
    }
  };

  const searchGroup = (searchTxt) => {
    let params = {
      page: 1,
      rowsperpage: 10,
    };
    if (searchTxt !== "") {
      params.textsearch = searchTxt;
    }
    // chuyển về view list group
    history.push(`/groups`);
    // search list group
    getListGroup(params)
      .then((res) => {
        if (res.status === 200) {
          console.log("search ne: ", searchTxt, res.data);
          setListGroup(res.data.data.groups);
          setTotal(res.data.data.total);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    // my group
    getGroupOfUser()
      .then((res) => {
        console.log("my group", res);
        if (res.status === 200) {
          let result = res.data;
          setListMyGroup(result.data.groups);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <React.Fragment>
      <div className={classes.heading}>
        <h3 className="heading-filter__fs15">Groups</h3>
        <div>
          <button
            type="button"
            onClick={() => {
              history.push(`/groups/create`);
            }}
          >
            <FontAwesomeIcon icon="plus-square" size="sm" color="#FFA500" />
          </button>
        </div>
      </div>

      <div className="search-bar-new" style={{display:"flex", width:"100%"}}>
        <input
          type="text"
          className="input-search-bar"
          placeholder="Search"
          onChange={(e) => setSearch(e.target.value)}
          value={search}
        />
        <button
          type="button"
          onClick={() => searchGroup(search)}
          style={{position:"relative", left:"-10%"}}
        >
          <FontAwesomeIcon icon="search" size="1x" color="#FFF" />
        </button>
      </div>
      <div>Groups You've Joined</div>
      <div>
        <div className="list-my-group">
          {listMyGroup &&
            listMyGroup.map((group) => (
              <div
                className="chip-group-item"
                onClick={() => pressBtnChip(group, currentType, setCurrentType)}
                key={group._id}
              >
                <Chip
                  linkAvatar={group.avatar}
                  label={group.name}
                  chipStyle={currentType === group._id ? "active" : ""}
                />
              </div>
            ))}
        </div>
      </div>
    </React.Fragment>
  );
}
