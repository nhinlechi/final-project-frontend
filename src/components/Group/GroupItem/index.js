import React from "react";
import Image from "../../UI/Image/Image";
import classes from "./GroupItem.module.css"

export default function GroupItem(props) {
    return (
        <div className={classes.wrapper}>
            {/* <div className="group-item-img" style={{backgroundImage: `url(${props.bgImage})`}}>
            </div>
            <Card.Body>
                <Card.Title className="group-item-name">{ props.groupName }</Card.Title>
            </Card.Body> */}
            <Image alt={props.groupName} src={props.bgImage}/>
            <div style={{display:"flex", flexDirection:"column"}}>{props.groupName} </div>
        </div>
    );
}
