import React, { useState, useEffect} from 'react';
import { useParams, useHistory } from 'react-router-dom';
import {
    Col,
    Row,
    Form,
    Button
} from 'react-bootstrap';
import { useForm, Controller } from 'react-hook-form';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import EarthImg from '../../../assets/images/Ellipse_36.jpg';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import TimerIcon from '@material-ui/icons/Timer';
import PlaceIcon from '@material-ui/icons/Place';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DatePicker from 'react-datepicker';
import LoadingSVG from '../../../assets/images/Rolling-1s-20px.svg';
import "react-datepicker/dist/react-datepicker.css";
import Tag from '../../Tag/index';
import PlaceItem from './PlaceItem';
import { ToastContainer, toast } from 'react-toastify';
import usePlaceSearch from './searchPlace';
import PageLoading from '../../PageLoading/pageLoading';

// api
import { updateEvent, getEventById } from '../../../api/eventApi';
import { getGroupById } from '../../../api/groupApi';

export default function EventEdit(props) {
    const history = useHistory();
    const params = useParams();
    const groupId = params.groupId;
    const eventId = params.eventId;

    // state
    const [groupInfo, setGroupInfo] = useState();
    const [eventInfo, setEventInfo] = useState();    

    // state
    const [message, setMessage] = useState('');
    const [startDate, setStartDate] = useState(new Date());
    const [placeChosen, setPlaceChosen] = useState();
    const [placeType, setPlaceType] = useState('restaurant');
    const [pageLoading, setPageLoading] = useState(false);
    // dialog

    const [open, setOpen] = useState(false);
    const handleClickOpen = () => {
        setOpen(true);
        console.log(123);
    };
    const handleClose = () => {
        setOpen(false);
    };
    const handleCloseChoosePlace = () => {
        setPlaceChosen(eventInfo.placeid);
        setOpen(false);
    };

    const [query, setQuery] = useState('')
    const [pageNumber, setPageNumber] = useState(1)

    const {
        loading,
        error,
        places,
        hasMore
    } = usePlaceSearch(query, placeType, pageNumber)

    console.log("hasmore: ", hasMore);

    // validate
    const { register, errors, handleSubmit, control, reset } = useForm();

    const onSubmit = async (formValues) => {
        setMessage("");
        // validate place
        if (!placeChosen) {
            setMessage("Please choose place!");
            return;
        }
        let data = {
            eventid: eventInfo._id,
            name: formValues.event_title,
            description: formValues.event_description,
            date: startDate.getTime(), // timestamp
            placeid: placeChosen._id
        };

        console.log(data)
        
        updateEvent(data, groupInfo._id) 
            .then((res) => {
                console.log("res update event", res)
                if (res.status === 200) {
                    console.log("update event complete", res)
                    // noti
                    toast.success('Update event success!', {
                        position: "top-right",
                        autoClose: 2000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        pauseOnHover: false,
                        draggable: true,
                        progress: undefined,
                    });
                    setEventInfo(res.data.data);
                }
            })
            .catch((err) => {
                console.log("err create event", err)
                setMessage("Time must be after minimum 30 minutes so with current");
            })
    }

    const getEventInfo = async () => {
        await getEventById(groupId, eventId)
            .then((res) => {
                console.log("event info: ", res.data)
                if (res.status === 200) {
                    setEventInfo(res.data.data);
                    setStartDate(new Date(res.data.data.date));
                    setPlaceChosen(res.data.data.placeid);
                }
            })
            .catch((err) => {
                console.log("get event info err", err);
            });
    }
    useEffect(() => {
        setPageLoading(true);
        const getGroupDetailInfo = async () => {
            await getGroupById(groupId)
                .then((res) => {
                    console.log("group info: ", res.data)
                    if (res.status === 200) {
                        setGroupInfo(res.data.data);
                        getEventInfo();
                        setPageLoading(false);
                    }
                })
                .catch((err)=> {
                    setPageLoading(false);
                    console.log("get group info err", err);
                })
        };
        getGroupDetailInfo();
    }, [groupId, eventId])

    return (
        <React.Fragment>
            { groupInfo && eventInfo && !pageLoading
            ?
            <React.Fragment>
                <div className="btn-return-list-group">
                    <div className="btn-like-group mr-2">
                        <button type="button" onClick={() => {
                            history.push(`/groups/${groupId}/event/detail/${eventId}`)
                        }}>
                            <ArrowBackIcon color="primary" />
                        </button>
                    </div>
                </div>
                <div className="create-group-event">
                    <div className="event-top">
                        <div className="icon-event-of-group mr-2">
                            <img src={EarthImg} alt="" />
                        </div>
                        <div className="text-event-of-group">
                            <div>
                                Edit event for
                            </div>
                            <div>
                                {groupInfo.name}
                            </div>
                        </div>
                    </div>
                    <div className="event-body mt-4">
                        <Form onSubmit={handleSubmit(onSubmit)}>
                            <Row className="mt-2">
                                <Col>
                                    <Form.Group controlId="fmEventTitle" style={{ width: "100%" }}>
                                        <Form.Label>Tilte</Form.Label>
                                        <Form.Control type="text" placeholder="Enter event title" name="event_title"
                                            ref={register({
                                                required: 'Event tilte is required'
                                            })} 
                                            defaultValue={eventInfo.name}
                                        />
                                        {errors.event_title && (
                                            <p className="errorMsg">{errors.event_title.message}</p>
                                        )}
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col>
                                    <Form.Group controlId="fmEventDesc">
                                        <Form.Label>Description</Form.Label>
                                        <Form.Control as="textarea" rows={6} placeholder="Enter description" name="event_description"
                                            ref={register({
                                                required: 'Event description is required'
                                            })} 
                                            defaultValue={eventInfo.description}    
                                        />
                                        {errors.event_description && (
                                            <p className="errorMsg">{errors.event_description.message}</p>
                                        )}
                                    </Form.Group>
                                </Col>
                            </Row>
                            <Row className="mt-2">
                                <Col md={4}>
                                    <Form.Label>Time</Form.Label>
                                    <div className="d-flex justify-content-between">
                                        <div className="mr-2" style={{ width: "100%" }}>
                                            <Form.Group controlId="fmEventTime" style={{ width: "100%" }}>
                                                {/* <Form.Control type="text" placeholder="18:00 AM" name="event_time" /> */}
                                                <Controller
                                                    // as={ReactDatePicker}
                                                    name={"event_time"}
                                                    control={control}
                                                    render={({ onChange, value }) => (
                                                        <DatePicker
                                                            className="datepick_custom"
                                                            name="event_time"
                                                            selected={startDate}
                                                            onChange={(date) => setStartDate(date)}
                                                            timeInputLabel="Time:"
                                                            dateFormat="dd/MM/yyyy h:mm aa"
                                                            showTimeInput
                                                            minDate={new Date()}
                                                        />
                                                    )}
                                                />
                                            </Form.Group>
                                        </div>
                                        <div className="">
                                            <Button type="button" className="btn_ffa500">
                                                <TimerIcon fontSize="small"/>
                                            </Button>
                                        </div>
                                    </div>
                                    {errors.event_time && (
                                        <p className="errorMsg">{errors.event_time.message}</p>
                                    )}
                                </Col>
                                <Col md={8}>
                                    <Form.Label>Place</Form.Label>
                                    <div className="d-flex justify-content-between">
                                        <div className="fake_input mr-2" >
                                            {placeChosen ? placeChosen.name : ""}
                                        </div>
                                        <div className="">
                                            <Button type="button" className="btn_ffa500" onClick={handleClickOpen}>
                                                <PlaceIcon fontSize="small" />
                                            </Button>
                                        </div>
                                    </div>
                                    {errors.event_place && (
                                        <p className="errorMsg">{errors.event_place.message}</p>
                                    )}
                                </Col>
                            </Row>
                            { message !== '' ? <p className="errorMsg">{message}</p> : "" }
                            <Row className="mt-2">
                                <Col>
                                    <Button type="submit" style={{ width: "100%" }} className="btn_ffa500">
                                        Update Event
                                    </Button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                    <Dialog
                        open={open}
                        onClose={handleClose}
                        scroll={"paper"}
                        fullWidth={true}
                        aria-labelledby="scroll-dialog-title"
                        aria-describedby="scroll-dialog-description"
                    >
                        <DialogTitle id="scroll-dialog-title">Chọn địa điểm</DialogTitle>
                        <DialogContent>
                            <div className="event-choose-places">
                                {/* Search bar */}
                                <div className="event-search-bar">
                                    <FontAwesomeIcon icon="search" size="1x" color="#858585" className="ml-3" />
                                    <input
                                        style={{ marginLeft: "1vh", marginTop: "1vh" }}
                                        type="text"
                                        className="search-places-input"
                                        placeholder="Enter search"
                                        value={query}
                                        onChange={e => {
                                            setQuery(e.target.value);
                                            setPageNumber(1);
                                        }}
                                    />
                                </div>
                                <div className="event-search-filter mt-3">
                                    <Tag
                                        text="Restaurant"
                                        bgColor={placeType === 'restaurant' ? "#FFA500": "#C4C4C4"}
                                        textColor="#FFF"
                                        onClick={() => {
                                            setPlaceType('restaurant');
                                            setPageNumber(1);
                                        }}
                                    />
                                    <Tag
                                        text="Bar"
                                        bgColor={placeType === 'bar' ? "#FFA500": "#C4C4C4"}
                                        textColor="#FFF"
                                        onClick={() => {
                                            setPlaceType('bar');
                                            setPageNumber(1);
                                        }}
                                    />
                                    <Tag
                                        text="Coffee"
                                        bgColor={placeType === 'cafe' ? "#FFA500": "#C4C4C4"}
                                        textColor="#FFF"
                                        onClick={() => {
                                            setPlaceType('cafe');
                                            setPageNumber(1);
                                        }}
                                    />
                                    <Tag
                                        text="Shopping mall"
                                        bgColor={placeType === 'shopping_mall' ? "#FFA500": "#C4C4C4"}
                                        textColor="#FFF"
                                        onClick={() => {
                                            setPlaceType('shopping_mall');
                                            setPageNumber(1);
                                        }}
                                    />
                                </div>
                                <div className="event-list-places">
                                    { places && 
                                        places.map((place) => (
                                            <PlaceItem key={place._id} 
                                                placeInfo={place} 
                                                selectedValue={placeChosen}
                                                setSelectedValue={setPlaceChosen}
                                            />
                                        ))
                                    }

                                    { hasMore === true 
                                        ? 
                                        <div className="mt-2">
                                            <Button type="button" variant="secondary" className="btn-block"
                                                onClick={() => setPageNumber(pageNumber + 1)}
                                            >
                                                {loading ? <img src={LoadingSVG} alt="loading place" /> : "Load more"}
                                            </Button>
                                        </div>
                                        : ""
                                    }
                                </div>
                            </div>
                        </DialogContent>
                        <DialogActions>
                            <Button type="button" variant="secondary" onClick={handleCloseChoosePlace}>
                                Cancel
                            </Button>
                            <Button type="button" className="btn_ffa500" onClick={handleClose}>
                                Choose
                            </Button>
                        </DialogActions>
                    </Dialog>

                    <ToastContainer />
                    
                </div>
            </React.Fragment>
            : <PageLoading />
            }
        </React.Fragment>
    );
}