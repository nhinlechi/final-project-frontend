import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import {
    Col,
    Row,
    Button
} from 'react-bootstrap';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import PlaceIcon from '@material-ui/icons/Place';
import StarIcon from '@material-ui/icons/Star';
import EditIcon from '@material-ui/icons/Edit';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import PageLoading from '../../PageLoading/pageLoading';
import ChatEvent from './ChatEvent/ChatEvent';

// api
import { getEventById, sendReqJoinEvent } from '../../../api/eventApi';
import { getGroupById } from '../../../api/groupApi';

import ImageDefaultPlace from '../../../assets/images/default-img-place.jpg';

export default function EventDetail(props) {
    const history = useHistory();
    const params = useParams();
    const groupId = params.groupId;
    const eventId = params.eventId;
    const USER_ID = localStorage.getItem("userid");
    // state
    const [groupInfo, setGroupInfo] = useState();
    const [eventInfo, setEventInfo] = useState();
    const [dateOfEvent, setDateOfEvent] = useState(new Date());
    const [pageLoading, setPageLoading] = useState(false);

    const checkJoinEvent = () => {
        let check = eventInfo.participants.find(p => p._id === USER_ID);
        console.log("check join event", check);
        if (check) return true
        return false;
    }

    const checkJoinGroup = () => {
        let check = groupInfo.participant.find(p => p._id === USER_ID);
        console.log("check join event", check);
        if (check) return true
        return false;
    }

    const submitReqJoin = async (status) => {
        let data = {
            status: status,
            eventid: eventInfo._id
        };
        
        await sendReqJoinEvent(data, groupInfo._id)
            .then((res) => {
                console.log("res send req join" ,res);
                if (res.status === 200) {
                    setEventInfo(res.data.data);
                    if (status) {
                        toast.success("Successful event participation")
                    } else {
                        toast.success("Leaving the event successfully")
                    }
                }
            })
            .catch((err) => {
                console.log("err send req join" ,err);
            });
    }

    const getEventInfo = async () => {
        await getEventById(groupId, eventId)
            .then((res) => {
                console.log("event info: ", res.data)
                if (res.status === 200) {
                    setEventInfo(res.data.data);
                    console.log(new Date(res.data.data.date))
                    setDateOfEvent(new Date(res.data.data.date))
                }
            })
            .catch((err) => {
                console.log("get event info err", err);
            });
    }
    useEffect(() => {
        setPageLoading(true);
        const getGroupInfo = async () => {
            await getGroupById(groupId)
                .then((res) => {
                    console.log("group info: ", res.data)
                    if (res.status === 200) {
                        setGroupInfo(res.data.data);
                        getEventInfo();
                        setPageLoading(false);
                    }
                })
                .catch((err)=> {
                    setPageLoading(false);
                    console.log("get group info err", err);
                })
        };
        getGroupInfo();
    }, [groupId, eventId])

    return (
        <React.Fragment>
        {  eventInfo && groupInfo && !pageLoading
            ?
            <React.Fragment>
                <div className="btn-return-list-group">
                    <div className="btn-like-group">
                        <button type="button" onClick={() => {
                            history.push(`/groups/detail/${groupId}`)
                        }}>
                            <ArrowBackIcon color="primary" />
                        </button>
                    </div>
                </div>

                <Row className="detail-group-event">
                    <Col className="detail-event-left">
                        <div className="detail-event-name">
                            <div className="txt-event_20px mr-3">
                                {eventInfo.name}
                            </div>
                            { groupInfo.host._id === USER_ID
                                ?
                                <div className="event-btn">
                                    <div className="btn-edit-event mr-2">
                                        <button type="button" onClick={() => {
                                            history.push(`/groups/${groupId}/event/edit/${eventId}`)
                                        }}>
                                            <EditIcon fontSize="small" style={{ color: "#FFF" }} />
                                        </button>
                                    </div>
                                </div>
                                :
                                ""
                            }
                        </div>
                        <div className="detail-event-description mt-2">
                            {eventInfo.description}
                        </div>
                        <div className="detail-event-time mt-2">
                            <div className="txt-event_20px">
                                Event time
                            </div>
                            <div className="time-content d-flex">
                                <div className="tag mt-2">
                                    <button
                                        type="button"
                                        className="tag-button"
                                        style={{ backgroundColor: "#C4C4C4", color: "#FFF" , fontSize: "15px"}}
                                    >
                                        {dateOfEvent.getHours() + ":" + dateOfEvent.getMinutes()}
                                    </button>
                                    -
                                    <button
                                        type="button"
                                        className="tag-button ml-2"
                                        style={{ backgroundColor: "#C4C4C4", color: "#FFF" , fontSize: "15px"}}
                                    >
                                        {dateOfEvent.getDate() + "/" + (dateOfEvent.getMonth() + 1) + "/" + dateOfEvent.getFullYear()}
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="detail-event-place mt-2">
                            <div className="txt-event_20px">
                                Event place
                            </div>
                            <div className="place-content">
                                <div className="place-avatar">
                                    <img src={eventInfo.placeid.photos[0] ? eventInfo.placeid.photos[0] : ImageDefaultPlace} alt="avatar place" />
                                </div>
                                <Row className="place-info">
                                    <Col md={8}>
                                        <div className="txt-event_18px">
                                            {eventInfo.placeid.name}
                                        </div>
                                    </Col>
                                    <Col md={4} className="d-flex align-items-center justify-content-end">
                                        <PlaceIcon fontSize="small" style={{ color: "#FFA500" }} />
                                    </Col>
                                </Row>
                                <Row className="place-info">
                                    <Col md={8}>
                                        <div className="">
                                            {eventInfo.placeid.address}
                                        </div>
                                    </Col>
                                    <Col md={4}>
                                        <div className="places-info-rate">
                                            <div className="places-info-rate-star mr-4">
                                                {parseFloat(eventInfo.placeid.rating).toFixed(1)} <StarIcon fontSize="small" style={{ color: "#FFA500" }} />
                                            </div>
                                            <div className="places-info-rate-review">
                                                {eventInfo.placeid.user_ratings_total} reviews
                                            </div>
                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        </div>
                        <div className="detail-event-submit mt-2">
                            {
                                groupInfo.host._id === USER_ID
                                ?
                                    checkJoinEvent() 
                                    ? 
                                    <Button type="button" style={{ width: "100%" }} variant="secondary"
                                        onClick={() => submitReqJoin(false)}
                                    >
                                        Joined
                                    </Button>
                                    :
                                    <Button type="button" style={{ width: "100%" }} className="btn_ffa500"
                                        onClick={() => submitReqJoin(true)}
                                    >
                                        I will come
                                    </Button>
                                :
                                    checkJoinGroup()
                                    ?
                                        checkJoinEvent() 
                                        ? 
                                        <Button type="button" style={{ width: "100%" }} variant="secondary"
                                            onClick={() => submitReqJoin(false)}
                                        >
                                            Joined
                                        </Button>
                                        :
                                        <Button type="button" style={{ width: "100%" }} className="btn_ffa500"
                                            onClick={() => submitReqJoin(true)}
                                        >
                                            I will come
                                        </Button>
                                    : ""
                            }
                        </div>
                    </Col>
                    <Col className="detail-event-right">
                        <ChatEvent eventInfo={eventInfo}/>
                    </Col>
                </Row>
                <ToastContainer
                    position="top-right"
                    autoClose={2000}
                    hideProgressBar
                    newestOnTop={false}
                    closeOnClick
                    rtl={false}
                    pauseOnFocusLoss
                    draggable
                    pauseOnHover
                />
            </React.Fragment>
            : <PageLoading />
            }
        </React.Fragment>
    )
}