import React from 'react';
import StarIcon from '@material-ui/icons/Star';
import Radio from '@material-ui/core/Radio';

import ImageDefaultPlace from '../../../assets/images/default-img-place.jpg'

import { withStyles } from '@material-ui/core/styles';
const TogetherRadio = withStyles({
    root: {
        color: "#FFA500",
        '&$checked': {
            color: '#FFA500',
        },
    },
    checked: {},
})((props) => <Radio color="default" {...props} />);

export default function PlaceItem({placeInfo, selectedValue, setSelectedValue}) {
    // radio
    const handleChange = () => {
        console.log(placeInfo);
        setSelectedValue(placeInfo);
    };

    return (
        <div className="event-places-item mt-2">
            <div className="places-item-avatar">
                <img src={placeInfo.photos[0] ? placeInfo.photos[0] : ImageDefaultPlace} alt="ahih" />
            </div>
            <div className="places-item-content">
                <div className="places-item-name">
                    {placeInfo.name}
                </div>
                <div className="places-item-rate">
                    <div className="item-rate-star mr-4">
                        5 <StarIcon fontSize="small" style={{ color: "#FFA500" }} />
                    </div>
                    <div className="item-rate-review">
                        10 reviews
                </div>
                </div>
                <div className="places-item-address">
                    {placeInfo.address}
                </div>
            </div>
            <div className="places-item-choose_radio">
                <div className="circle_outline-radio">
                    <TogetherRadio
                        checked={selectedValue._id === placeInfo._id}
                        onChange={handleChange}
                        value={placeInfo._id}
                        name="place_id"
                        color="default"
                        style={{ height: "2rem", width: "2rem" }}
                    />
                </div>
            </div>
        </div>
    );
}