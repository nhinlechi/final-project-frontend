import React from 'react';
import { useParams, useHistory } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import RoomIcon from '@material-ui/icons/Room';

export default function EventItem({ event }) {
    const history = useHistory();
    const params = useParams();
    const groupId = params.groupId;

    const chooseItem = () => {
        history.push(`/groups/${groupId}/event/detail/${event._id}`)
    }
    return (
        <div className="event-item mt-2" onClick={() => chooseItem()}>
            <div className="event-item-left" style={{ width: "70%" }}>
                <div className="event-name">
                    {event.name}
                </div>
                <div className="mt-1 event-address align-items-center">
                    <RoomIcon color="primary" />
                    {event.placeid.address}
                </div>
            </div>
            <div className="event-item-right">
                <div className="event-time">
                    20:00 - 01/06/2021
                </div>
                <div className="event-participants">
                    <AvatarGroup max={3} >
                        {event.participants.length > 0
                            ?
                            event.participants.map((item) => (
                                <Avatar alt={item._id} key={"evt" + item._id} src={item.avatar.length > 0 ? item.avatar[0] : ""} />
                            ))
                            : null
                        }
                    </AvatarGroup>
                </div>
            </div>
        </div>
    );
}