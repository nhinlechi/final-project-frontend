import { useEffect, useState } from 'react'
import { getSearchPlace } from '../../../api/placeApi';

const ROW_PER_PAGE = 10;

export default function PlaceSearch(query, type, pageNumber) {
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(false)
    const [places, setPlaces] = useState([])
    const [hasMore, setHasMore] = useState(false)

    useEffect(() => {
        setPlaces([])
    }, [query, type])

    useEffect(() => {
        // console.log(`query: ${query}, type: ${type}, pageNumber: ${pageNumber}`);
        setLoading(true)
        setError(false)
        console.log("pagenumber", pageNumber);
        getSearchPlace(query, type, pageNumber, ROW_PER_PAGE)
            .then(res => {
                console.log(res);
                setPlaces(prevPlaces => {
                    return [...new Set([...prevPlaces, ...res.data.data.places.map(p => p)])]
                })
                setHasMore(res.data.data.places.length > 0)
                setLoading(false)
            }).catch(e => {
                setError(true)
            })
    }, [query, type, pageNumber])

    return { loading, error, places, hasMore }
}