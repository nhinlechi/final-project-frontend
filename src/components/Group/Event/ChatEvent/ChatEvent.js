import React, { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useParams } from 'react-router-dom';
import EarthImg from '../../../../assets/images/Ellipse_36.jpg';

import MessageItem from '../../../MessageItem';
import useChatEvent from './useChatEvent';
// api
import { getChatEvent } from '../../../../api/eventApi';

const ChatEvent = (props) => {
    const params = useParams();
    const messagesEndRef = React.createRef();
    const eventId = params.eventId;
    const groupId = params.groupId;

    const [listMessage, setListMessage] = useState([]);
    /**
     * Custom hook, handle send chat and receive new message
     */
    const { messages, sendMessage } = useChatEvent(eventId, listMessage);
    const [currentMessage, setCurrentMessage] = useState("");

    /**
     * Get list message
     */
    useEffect(() => {
        if (props.eventInfo) {
            getChatEvent({ groupid: groupId, eventid: eventId })
                .then((res) => {
                    console.log("list message", res);
                    if (res.status === 200) {
                        console.log(res.data.data);
                        setListMessage(res.data.data.reverse());
                    }
                })
                .catch((err) => {
                    console.log(err);
                })
        }

    }, [eventId])

    /**
     * JSX list message
     */
    const messageHandler = messages.map((element) => {
        const message = element.data;
        const userid = localStorage.getItem("userid");
        const isMine = element.sender === userid;
        let time = new Date(element.created);
        return < MessageItem
            key={element.created}
            text={message}
            title={time.toString()}
            sender={isMine ? "mine" : ""}
        />
    })

    /**
     * Auto scrool when having new message
     */
    const scrollToBottom = () => {
        messagesEndRef.current.scrollIntoView({ behavior: 'smooth' })
    }
    useEffect(() => {
        scrollToBottom();
    }, [messages])

    /**
     * Send chat handler 
     * @param {Event} e 
     */
    const sendChatClickHandler = (e) => {
        e.preventDefault();
        sendMessage(currentMessage);
        setCurrentMessage("");
    }


    return (
        <React.Fragment>
            <div className="event-detail-frame-chat">
                <div className="top-header-chat">
                    <div className="detail-event-chat_header">
                        <div className="icon-event-of-group mr-2">
                            <img src={EarthImg} alt="" />
                        </div>
                        <div className="text-event-of-group">
                            <div className="txt-event_20px">
                                How do you thing about this Event ?
                            </div>
                        </div>
                    </div>
                </div>
                <div className="content-chat-view">
                    {messageHandler}
                    <div ref={messagesEndRef} />
                </div>
                <div className="compose-input-text">
                    <input type="text" className="compose-input" placeholder="Type a message" 
                        onChange={(e) => { setCurrentMessage(e.target.value) }} value={currentMessage}
                    />
                    <div className="compose-btn">
                        <button onClick={sendChatClickHandler} type="button">
                            <FontAwesomeIcon icon="paper-plane" size="1x" color="#FFA500" />
                        </button>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}

export default ChatEvent;