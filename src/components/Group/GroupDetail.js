import React, { useEffect, useState } from "react";
import { Col, Row, Table } from "react-bootstrap";
import { useParams, useHistory } from "react-router-dom";
import Avatar from "@material-ui/core/Avatar";
import AvatarGroup from "@material-ui/lab/AvatarGroup";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import AppBar from "@material-ui/core/AppBar";
import Tab from "@material-ui/core/Tab";
import TabContext from "@material-ui/lab/TabContext";
import TabList from "@material-ui/lab/TabList";
import TabPanel from "@material-ui/lab/TabPanel";
import { makeStyles } from "@material-ui/core/styles";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Tag from "../../components/Tag/index";
import PageLoading from "../PageLoading/pageLoading";
import EventItem from "./Event/EventItem";
import ImgNoEvent from "../../assets/images/group_event.png";

// api
import { sendReqJoinGroup, getGroupById, leaveGroup } from "../../api/groupApi";
import { getUserById } from "../../api/userApi";
import { getListEvent } from "../../api/eventApi";
import Image from "../UI/Image/Image";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.paper,
  },
  appBarCustom: {
    color: "#FFA500",
    backgroundColor: "#FFF",
  },
  tabRoot: {
    textTransform: "none",
    minWidth: "60px",
  },
  avatarRoot: {
    width: "30px",
    height: "30px",
  },
  tabpanelRoot: {
    padding: "10px",
  },
}));

export default function GroupDetail(props) {
  const userid = localStorage.getItem("userid");
  const history = useHistory();
  const classes = useStyles();
  const params = useParams();
  const groupId = params.groupId;

  // state
  const [groupInfo, setGroupInfo] = useState();
  const [hostInfo, setHostInfo] = useState("");
  const [reqJoin, setReqJoin] = useState(false);
  const [reasonJoin, setReasonJoin] = useState("");
  const [openDialog, setOpenDialog] = useState(false);
  const [value, setValue] = useState("1"); // switch tab list event or host info
  const [loading, setLoading] = useState(false); // loading page
  const [eventList, setEventList] = useState([]);

  const handleClickOpen = () => {
    setOpenDialog(true);
  };
  const handleClose = () => {
    setOpenDialog(false);
  };
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  // get host info
  const getHostInfo = async (hostId) => {
    await getUserById(hostId)
      .then((res) => {
        console.log("host info: ", res.data);
        if (res.status === 200) {
          setHostInfo(res.data.data);
        }
      })
      .catch((err) => {
        console.log("get host info err", err);
      });
  };
  // get event of group
  const getEvent = async (groupId) => {
    let params = {
      groupid: groupId,
      page: 1,
      rowsperpage: 5,
    };
    await getListEvent(params, groupId)
      .then((res) => {
        console.log("get list event", res);
        if (res.status === 200) {
          setEventList(res.data.data.events);
        }
      })
      .catch((err) => {
        console.log("get list event", err);
      });
  };

  // Request join group
  const pressReqJoin = async (groupId, reasonJoin) => {
    let reqBody = {
      groupid: groupId,
      description: reasonJoin ? reasonJoin : "",
    };
    await sendReqJoinGroup(reqBody)
      .then((res) => {
        if (res.status === 200) {
          console.log("req join:", res);
          setReqJoin(true);
          setOpenDialog(false);
        }
      })
      .catch((err) => {
        console.log("req join err: ", err);
        setOpenDialog(false);
      });
  };
  // leave group
  const pressLeaveGroup = async () => {
    await leaveGroup({ groupid: groupId })
      .then((res) => {
        console.log("req leave:", res);
        if (res.status === 200) {
          setReqJoin(false);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    setLoading(true);
    const getGroupDetailInfo = async () => {
      await getGroupById(groupId)
        .then((res) => {
          console.log("group info: ", res.data);
          if (res.status === 200) {
            setGroupInfo(res.data.data);
            getHostInfo(res.data.data.host._id);
            getEvent(res.data.data._id);
            setLoading(false);
            // check join group
            const is_join = res.data.data.participant.filter(
              (p) => p._id === userid
            );
            if (is_join.length > 0) {
              setReqJoin(true);
            }
          }
        })
        .catch((err) => {
          setLoading(false);
          console.log("get group info err", err);
        });
    };
    getGroupDetailInfo();
  }, [groupId]);

  return (
    <React.Fragment>
      {groupInfo && !loading ? (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            maxHeight: "100vh",
            width: "100%",
          }}
        >
          <div className="btn-return-list-group">
            <div className="btn-like-group mr-2">
              <button
                type="button"
                onClick={() => {
                  history.push("/groups");
                }}
              >
                <ArrowBackIcon color="primary" />
              </button>
            </div>
          </div>
          <div className="group-detail-top">
            {/* AVATAR */}
            <div className="group-detail-avatar">
              <Image
                src={groupInfo.avatar}
                alt="group detail avatar"
                style={{ objectFit: "contain" }}
              />
            </div>
            {/* BUTTON */}
          </div>
          <div
            className="group-detail-bot mt-3"
            style={{ display: "flex", justifyContent: "space-between" }}
          >
            <div sm={6}>
              <div className="bot_left">
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <div className="txt_l-s16">
                    {groupInfo.name !== null ? groupInfo.name : ""}
                  </div>
                  {userid === groupInfo.host._id ? (
                    // START: View of host
                    <div>
                      <div className="btn-like-group d-flex justify-content-center mr-2">
                        <button
                          type="button"
                          onClick={() => {
                            history.push(`/groups/edit/${groupId}`);
                          }}
                        >
                          <FontAwesomeIcon
                            icon="edit"
                            size="sm"
                            color="#FFA500"
                          />
                        </button>
                      </div>
                    </div>
                  ) : (
                    //  START: View of viewer and member
                    <div>
                      <div className="btn-like-group d-flex justify-content-center">
                        {reqJoin === false ? (
                          <button type="button" onClick={handleClickOpen}>
                            <FontAwesomeIcon
                              icon="plus-square"
                              size="sm"
                              color="#FFA500"
                            />
                          </button>
                        ) : (
                          <button type="button" onClick={pressLeaveGroup}>
                            <FontAwesomeIcon
                              icon="arrow-alt-circle-right"
                              size="sm"
                              color="#FFA500"
                            />
                          </button>
                        )}
                      </div>
                    </div>
                  )}
                </div>
                <div className="d-flex justify-content-start">
                  <div className="group-type mr-3">
                    <div className="group-privacy">
                      {groupInfo.isprivate === true
                        ? "Private Group"
                        : "Public group"}
                    </div>
                    <div className="group-number-member">
                      {groupInfo.participant.length} thành viên
                    </div>
                  </div>
                  <div className="avt-member">
                    <AvatarGroup max={4}>
                      {groupInfo.participant.length > 0
                        ? groupInfo.participant.map((item) => (
                            <Avatar
                              alt={item._id}
                              key={"avt" + item._id}
                              src={item.avatar.length > 0 ? item.avatar[0] : ""}
                            />
                          ))
                        : null}
                    </AvatarGroup>
                  </div>
                </div>

                <div className="group-tag mt-3">
                  {groupInfo.tag &&
                    groupInfo.tag.map((tag) => (
                      <Tag
                        bgColor={"#C4C4C4"}
                        textColor={"#FFF"}
                        text={tag.name}
                        key={tag._id}
                      />
                    ))}
                </div>
                <div className="group-description mt-3">
                  Description
                  <p>
                    {groupInfo.description !== null
                      ? groupInfo.description
                      : ""}
                  </p>
                </div>
              </div>
            </div>
            <div sm={6} style={{ display: "flex", width: "50%" }}>
              <div className="bot_right" style={{ width: "80%" }}>
                <div className="number_likes">
                  <span className="txt_l-s16">
                    {" "}
                    {groupInfo.star !== null ? groupInfo.star : 0}{" "}
                  </span>
                  <FontAwesomeIcon icon="heart" size="lg" color="#FFA500" />
                </div>
                {userid === groupInfo.host._id ? (
                  <div className="event-info mt-3">
                    <div className="event__heading">
                      <div className="event-txt__heading">Events</div>
                      <div className="event-btn-create__heading">
                        <button
                          type="button"
                          className="custom-btn_ffa500"
                          onClick={() => {
                            history.push(`/groups/${groupId}/event/create`);
                          }}
                        >
                          Add new events
                        </button>
                      </div>
                    </div>
                    <div className="event-list">
                      {eventList.length > 0 ? (
                        eventList.map((event) => (
                          <EventItem key={event._id} event={event} />
                        ))
                      ) : (
                        <div
                          className="no-event mt-3"
                          style={{ textAlign: "center" }}
                        >
                          <img src={ImgNoEvent} alt="no vent" />
                          <div className="no-event-text">
                            Create group event for connecting people
                          </div>
                        </div>
                      )}
                    </div>
                  </div>
                ) : hostInfo ? (
                  // View of viewer and member
                  <div className={classes.root}>
                    <TabContext value={value}>
                      <AppBar
                        position="static"
                        className={classes.appBarCustom}
                        elevation={0}
                      >
                        <TabList
                          onChange={handleChange}
                          aria-label="simple tabs example"
                        >
                          <Tab
                            label="Events"
                            value="1"
                            className={classes.tabRoot}
                          />
                          <Tab
                            label="Host"
                            value="2"
                            className={classes.tabRoot}
                          />
                        </TabList>
                      </AppBar>
                      <TabPanel value="1" className={classes.tabpanelRoot}>
                        <div className="event-info">
                          <div className="event-list">
                            {eventList.length > 0 ? (
                              eventList.map((event) => (
                                <EventItem key={event._id} event={event} />
                              ))
                            ) : (
                              <div
                                className="no-event mt-3"
                                style={{ textAlign: "center" }}
                              >
                                <img src={ImgNoEvent} alt="no vent" />
                                <div className="no-event-text">No event</div>
                              </div>
                            )}
                          </div>
                        </div>
                      </TabPanel>
                      <TabPanel value="2" className={classes.tabpanelRoot}>
                        <div className="host-info">
                          <Table>
                            <tbody>
                              <tr className="boder-style_hidden">
                                <td className="pt-1">
                                  <div className="txt_s25">
                                    Host information
                                  </div>
                                  <div className="txt_s20">
                                    {`${hostInfo.firstname} ${hostInfo.lastname}`}
                                  </div>
                                </td>
                                <td className="pt-1 text-right">
                                  <Avatar
                                    alt="Remy Sharp"
                                    src={
                                      hostInfo.avatar.length > 0
                                        ? hostInfo.avatar[0]
                                        : ""
                                    }
                                  />
                                </td>
                              </tr>
                              <tr className="boder-style_hidden">
                                <td className="pt-1">Phone</td>
                                <td className="pt-1 ">{hostInfo.phone}</td>
                              </tr>
                              <tr className="boder-style_hidden">
                                <td className="pt-1">Gender</td>
                                <td className="pt-1">
                                  {hostInfo.gender === 1 ? "Nam" : "Nữ"}
                                </td>
                              </tr>
                              <tr className="boder-style_hidden">
                                <td className="pt-1">Relationship status</td>
                                <td className="pt-1">Single</td>
                              </tr>
                            </tbody>
                          </Table>
                        </div>
                      </TabPanel>
                    </TabContext>
                  </div>
                ) : null}
              </div>
            </div>
          </div>
          <Dialog
            open={openDialog}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
          >
            <DialogTitle id="form-dialog-title">
              The reason for wanting to join
            </DialogTitle>
            <DialogContent>
              <DialogContentText>
                To join to this group, please enter your reason here
              </DialogContentText>
              <TextField
                onChange={(e) => setReasonJoin(e.target.value)}
                value={reasonJoin}
                type="text"
                autoFocus
                margin="dense"
                id="reason"
                label="Reason"
                fullWidth
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClose} color="primary">
                Cancel
              </Button>
              <Button
                onClick={() => pressReqJoin(groupInfo._id, reasonJoin)}
                color="primary"
              >
                Join
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      ) : (
        <PageLoading />
      )}
    </React.Fragment>
  );
}
