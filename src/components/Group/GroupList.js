import React, { useEffect, useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import { GroupContext } from "../../context/GroupContext";
import GroupItem from "./GroupItem/index";
import PaginationGroup from "rc-pagination";
import PageLoading from "../PageLoading/pageLoading";

import { getListGroup } from "../../api/groupApi";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
// import Swiper core and required modules
import SwiperCore, { Pagination } from "swiper/core";

// install Swiper modules
SwiperCore.use([Pagination]);

const PAGE_SIZE = 8;

export default function GroupList(props) {
  const history = useHistory();
  // context
  const { listGrp, txtSearch, totalGroup } = useContext(GroupContext);
  const [listGroup, setListGroup] = listGrp;
  const textSearchGroup = txtSearch.search;
  const [total, setTotal] = totalGroup; // tổng số trang

  // state
  const [groupTopList, setGroupTopList] = useState([]);
  const [loading, setLoading] = useState(false);

  const [currentPage, setCurrentPage] = useState(1); // trang hiện tại

  function loadViewDetail(groupId) {
    // Lấy thông tin group
    history.push(`/groups/detail/${groupId}`);
  }

  const handleChangePage = (curPos) => {
    setCurrentPage(curPos);
  };

  // top group: hard page
  const getTopListGroup = async () => {
    let params = {
      page: 1,
      rowsperpage: PAGE_SIZE,
      textseach: textSearchGroup,
    };
    await getListGroup(params)
      .then((res) => {
        if (res.status === 200) {
          setGroupTopList(res.data.data.groups);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  useEffect(() => {
    getTopListGroup();
  }, []);

  useEffect(() => {
    setLoading(true);
    let params = {
      page: currentPage,
      rowsperpage: PAGE_SIZE,
      textseach: textSearchGroup,
    };
    // get group theo page
    getListGroup(params)
      .then((res) => {
        console.log(res);
        if (res.status === 200) {
          setListGroup(res.data.data.groups);
          setTotal(res.data.data.total);
          setLoading(false);
        }
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
  }, [currentPage]);

  const topGroupList = groupTopList.map((group) => (
    <div
      key={"top-group-" + group._id}
      className="top-group-item"
      style={{ display: "inline-block", width: "auto" }}
      onClick={() => loadViewDetail(group._id)}
    >
      <GroupItem
        groupName={group.name}
        groupId={group._id}
        bgImage={group.avatar}
      />
    </div>
  ));

  const listGroupJSX = listGroup.map((group) => (
    <div
      key={"list-group-" + group._id}
      onClick={() => loadViewDetail(group._id)}
    >
      <GroupItem
        groupName={group.name}
        groupId={group._id}
        bgImage={group.avatar}
      />
    </div>
  ));

  return (
    <React.Fragment>
      {loading ? (
        <PageLoading />
      ) : (
        <React.Fragment>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              width: "80%",
              maxHeight: "100vh",
            }}
          >
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                width: "100%",
              }}
            >
              <h2>Top group</h2>
              <div
                className="list-top-group"
                style={{
                  whiteSpace: "nowrap",
                  overflowX: "scroll",
                  width: "90%",
                }}
              >
                {groupTopList && topGroupList}
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                width: "100%",
              }}
            >
              <h2>Group list</h2>
              <div style={{ display: "flex", flexWrap: "wrap", width: "100%" }}>
                {listGroup && listGroupJSX}
              </div>
              <PaginationGroup
                total={total}
                current={currentPage}
                pageSize={PAGE_SIZE}
                onChange={handleChangePage}
                style={{ marginTop: "1rem" }}
              />
            </div>
          </div>
        </React.Fragment>
      )}
    </React.Fragment>
  );
}
