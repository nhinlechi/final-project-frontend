import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function SearchBar(props) {
  return (
      <div className="search-bar" style={{display:"flex"}}>
        <div>
          <input
            style={{ marginLeft: "1vh", marginTop: "1vh" }}
            type="text"
            className="search-input"
            placeholder="Enter search"
            onChange={props.onChange}
            
          />
        </div>
        <div>
          <button
            className="search-button"
            onClick={props.onClick}
            style={{position:"relative", left:"-40%"}}
            autoFocus
          >
            <FontAwesomeIcon icon="search" size="1x" color="#858585" />
          </button>
        </div>
      </div>
  );
}
