import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import ModalMatch from "../../components/ModalMatch/index";
import Sidebar from "./Sidebar/index";
import LocationContext from "../../context/LocationContext";
import MatchContext from "../../context/MatchContext";
import { LoadingProvider } from "../../context/LoadingContext";
import { VideoCallProvider } from "../../context/VideoCallContext";
import VideoCallNotification from "../../components/VideoCall/notification/VideoCallNotification";
import classes from "./user-layout.module.css";
// api
import { saveFirebase } from "../../api/firebaseApi";

// context
import ProfileContext from "../../context/ProfileContext";
// api
import { getUserInfo } from "../../api/userApi";
import { RestaurantProvider } from "../../context/RestaurantContext";
// Hàm gọi api save firebase token
const saveFirebaseToken = async () => {
  let data = { tokenid: localStorage.getItem("firebaseToken") };
  await saveFirebase(data)
    .then((response) => {
      if (response.status !== 200) {
        console.log("error");
      }
    })
    .catch((error) => {
      console.log(error);
    });
};

export default function UserLayout(props) {
  let history = useHistory();
  // check login
  const token = localStorage.getItem("token");
  useEffect(() => {
    if (token == null) {
      history.push("/login");
    }
    saveFirebaseToken();
  }, []);

  const [location, setLocation] = useState({ latitude: 0, longitude: 0 });
  const [isProcessMatching, setIsProcessMatching] = useState(false);
  const [matchInfor, setMatchInfor] = useState({});

  const [dataProfile, setDataProfile] = useState();
  const [profilePage, setProfilePage] = useState("profile-detail");

  useEffect(() => {
    getUserInfo()
      .then((res) => {
        if (res.status === 200) {
          setDataProfile(res.data.data);
        }
        console.log(res);
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, []);

  function success(pos) {
    var crd = pos.coords;
    setLocation({
      latitude: crd.latitude,
      longitude: crd.longitude,
    });
  }
  function error(err) {
    console.warn(`ERROR(${err.code}): ${err.message}`);
  }

  const getLocationHandler = () => {
    navigator.geolocation.getCurrentPosition(success, error);
  };

  useEffect(() => {
    getLocationHandler();
  }, []);

  const setIsProcessMatchingHandler = (isProcess) => {
    setIsProcessMatching(isProcess);
  };

  const setMatchInforHandler = (matchInfor) => {
    setMatchInfor({
      fullname: matchInfor.fullname,
      avatar: matchInfor.avatar,
      notificationid: matchInfor.notificationid,
      timeout: matchInfor.timeout,
    });
  };

  return (
    <ProfileContext.Provider
      value={{
        dataProfile: dataProfile,
        setDataProfile: setDataProfile,
        profilePage: profilePage,
        setProfilePage: setProfilePage,
      }}
    >
      <RestaurantProvider>
        <LoadingProvider>
          <MatchContext.Provider
            value={{
              isProcessMatching: isProcessMatching,
              matchInfor: matchInfor,
              setIsProcessMatching: setIsProcessMatchingHandler,
              setMatchInfor: setMatchInforHandler,
            }}
          >
            <LocationContext.Provider value={{ location: location }}>
              <VideoCallProvider>
                <React.Fragment>
                  <div className={classes.wrapper}>
                    {/* Begin sidebar LEFT */}
                    <Sidebar />
                    {/* End sidebar LEFT */}
                    <div className={classes.content}>{props.children}</div>
                    <ModalMatch
                      isProcessMatching={isProcessMatching}
                      setIsProcessMatching={setIsProcessMatching}
                      userInformMatch={matchInfor}
                    />
                    <VideoCallNotification />
                  </div>
                </React.Fragment>
              </VideoCallProvider>
            </LocationContext.Provider>
          </MatchContext.Provider>
        </LoadingProvider>
      </RestaurantProvider>
    </ProfileContext.Provider>
  );
}
