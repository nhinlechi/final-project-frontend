import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import DialogContent from '@material-ui/core/DialogContent';
import Avatar from '@material-ui/core/Avatar';
import AccountBox from '@material-ui/icons/AccountBox';
import EmailIcon from '@material-ui/icons/Email';
import CakeIcon from '@material-ui/icons/Cake';
import PhoneIcon from '@material-ui/icons/Phone';
import Tag from '../../../../components/Tag/index';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import ProfileContext from '../../../../context/ProfileContext';

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css"
// import Swiper core and required modules
import SwiperCore, {
    Pagination
} from 'swiper/core';

// install Swiper modules
SwiperCore.use([Pagination]);

function renderSwitchGender(param) {
    switch (param) {
        case 0:
            return 'Unknow';
        case 1:
            return 'Nam';
        case 2:
            return 'Nữ';
        default:
            return 'Khác';
    };
}

function renderSwitchRelation(param) {
    switch (param) {
        case 0:
            return 'Unknow';
        case 1:
            return 'In relationship';
        case 2:
            return 'Alone';
        default:
            return 'Divorcee';
    };
}

function renderConvertAge(timestamp) {
    // timestamp to date
    let now = new Date();
    let dateOfBirth = new Date(timestamp);
    let yearOfBirth = dateOfBirth.getFullYear(dateOfBirth);
    let yearNow = now.getFullYear(now);
    if (yearOfBirth < yearNow) {
        return yearNow - yearOfBirth;
    } else {
        return yearOfBirth - yearNow;
    }
}

function renderConvertBirthday(timestamp) {
    const d = new Date(timestamp);
    return (d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear());
}

export default function ProfileDetail() {
    const history = useHistory();
    const profileContext = useContext(ProfileContext);
    const dataProfile = profileContext.dataProfile;
    console.log(dataProfile);
    const viewProfileEdit = () => {
        profileContext.setProfilePage("profile-edit");
    }

    const onClickLogout = () => {
        localStorage.clear();
        history.push("/login");
    }

    return (
        <React.Fragment>
            {dataProfile
                ?
                <React.Fragment>
                    <div className="profile-detail-top">
                        <div className="profile-btn-edit">
                            <button type="button" onClick={viewProfileEdit}>
                                <FontAwesomeIcon icon="user-edit" size="1x" color={"#FFF"} />&nbsp;Edit
                            </button>
                        </div>
                        <div className="btn-logout">
                            <button type="button" onClick={onClickLogout}>
                                <FontAwesomeIcon icon="sign-out-alt" style={{ height: "1.5rem", width: "1.5rem" }} />
                            </button>
                        </div>
                    </div>
                    <DialogContent>
                        <Swiper pagination={{
                            "dynamicBullets": true
                        }} className="mySwiper">
                            {dataProfile.avatar
                                ?
                                dataProfile.avatar.map((avatar, index) => (
                                    <SwiperSlide key={"avt-profile-" + index}>
                                        <div className="img-slide-profile">
                                            <img src={avatar} alt="est" />
                                        </div>
                                    </SwiperSlide>
                                ))
                                : null
                            }
                        </Swiper>
                        <div className="profile mt-2">
                            <div className="profile-top d-flex justify-content-between">
                                <div className="profile-name-age">
                                    <span className="profile-name" style={{ fontWeight: "700" }}>
                                        {dataProfile.lastname + " " + dataProfile.firstname}
                                    </span>
                                    <span className="profile-old">
                                        &nbsp;{renderConvertAge(dataProfile.dateofbirth)}
                                    </span>
                                </div>

                            </div>
                            <div className="profile-basic-info">
                                <div className="profile-text_22">
                                    Basic information
                                </div>
                                <div className="profile-content">
                                    <div className="card-mini-info">
                                        <div className="row mt-2">
                                            <div className="col-2 d-flex align-items-center">
                                                <Avatar>
                                                    <AccountBox />
                                                </Avatar>
                                            </div>
                                            <div className="col-10 d-flex flex-column" style={{ padding: 0 }}>
                                                <div className="text_20_b">{dataProfile.lastname + " " + dataProfile.firstname}</div>
                                                <div>Full name</div>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-2 d-flex align-items-center">
                                                <Avatar>
                                                    <AccountBox />
                                                </Avatar>
                                            </div>
                                            <div className="col-10 d-flex flex-column" style={{ padding: 0 }}>
                                                <div className="text_20_b">
                                                    {renderSwitchGender(dataProfile.gender)}
                                                </div>
                                                <div>Gender</div>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-2 d-flex align-items-center">
                                                <Avatar>
                                                    <CakeIcon />
                                                </Avatar>
                                            </div>
                                            <div className="col-10 d-flex flex-column" style={{ padding: 0 }}>
                                                <div className="text_20_b">
                                                    {renderConvertBirthday(dataProfile.dateofbirth)}
                                                </div>
                                                <div>Birthday</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="profile-hobbies">
                                <div className="profile-text_22">
                                    Favorites
                                </div>
                                <div className="profile-content">
                                    {dataProfile.favorites.length > 0
                                        ?
                                        dataProfile.favorites.map((fvr) => (
                                            <Tag key={"fvr-" + fvr._id} bgColor={"#858585"} text={fvr.name} textColor={"#FFF"} />
                                        ))
                                        : null
                                    }
                                </div>
                            </div>
                            <div className="profile-contact">
                                <div className="profile-text_22">
                                    Contact information
                                </div>
                                <div className="profile-content">
                                    <div className="card-mini-info">
                                        <div className="row mt-2">
                                            <div className="col-2 d-flex align-items-center">
                                                <Avatar>
                                                    <PhoneIcon />
                                                </Avatar>
                                            </div>
                                            <div className="col-10 d-flex flex-column" style={{ padding: 0 }}>
                                                <div className="text_20_b">{dataProfile.phone}</div>
                                                <div>Phone number</div>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-2 d-flex align-items-center">
                                                <Avatar>
                                                    <EmailIcon />
                                                </Avatar>
                                            </div>
                                            <div className="col-10 d-flex flex-column" style={{ padding: 0 }}>
                                                <div className="text_20_b">{dataProfile.username}</div>
                                                <div>Email</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="profile-relationship">
                                <div className="profile-text_22">
                                    Relationship status
                                </div>
                                <div className="profile-content">
                                    <div className="card-mini-info">
                                        <div className="row mt-2">
                                            <div className="col-2 d-flex align-items-center">
                                                <Avatar>
                                                    <AccountBox />
                                                </Avatar>
                                            </div>
                                            <div className="col-10 d-flex flex-column" style={{ padding: 0 }}>
                                                <div className="text_20_b">
                                                    {renderSwitchRelation(dataProfile.relationship)}
                                                </div>
                                                <div>Relationship</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </DialogContent>
                </React.Fragment>
                : null
            }
        </React.Fragment>
    )
}