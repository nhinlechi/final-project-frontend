import React, { useContext, useState } from 'react'
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import ProfileContext from '../../../../context/ProfileContext';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

// api
import { updateUser } from '../../../../api/userApi';

const arrayRela = [
    {
        value: 0,
        label: 'Unknow',
    },
    {
        value: 1,
        label: 'In relationship',
    },
    {
        value: 2,
        label: 'Alone',
    },
    {
        value: 3,
        label: 'Divorcee',
    },
];

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    }
}));

export default function RelaInfo(props) {
    const classes = useStyles();
    const profileContext = useContext(ProfileContext);
    const profileData = profileContext.dataProfile;
    const [rela, setRela] = useState(profileData.relationship);

    const handleChangeRela = (event) => {
        setRela(event.target.value);
    };

    const saveRelaInfo = async () => {
        // submit, set 
        await updateUser({relationship: rela})
            .then((res) => {
                console.log(res);
                if (res.status === 200) {
                    profileContext.setDataProfile(res.data.data);
                    profileContext.setProfilePage("profile-edit");
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }
    return (
        <React.Fragment>
            <DialogTitle id="draggable-dialog-title">
                <div className="d-flex justify-content-between">
                    Relationship status
                    <button type="button" className="btn_text_cancel" onClick={() => {
                        profileContext.setProfilePage("profile-edit");
                    }}>
                        Cacnel
                    </button>
                </div>
            </DialogTitle>
            <DialogContent>
                <form className={classes.root} noValidate autoComplete="off">
                    <TextField
                        id="relationship"
                        select
                        label="relationship"
                        style={{ margin: '8px 0px' }}
                        fullWidth
                        value={rela}
                        onChange={handleChangeRela}
                        variant="outlined"
                        size="small"
                    >
                        {arrayRela.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </form>
            </DialogContent>
            <DialogActions style={{ justifyContent: "center" }}>
                <div className="profile-edit-btn-save mt-2">
                    <button type="button" onClick={saveRelaInfo}>
                        SAVE
                    </button>
                </div>
            </DialogActions>
        </React.Fragment>
    )
}