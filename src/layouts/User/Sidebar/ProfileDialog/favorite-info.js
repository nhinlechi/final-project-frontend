import React, { useContext, useState, useEffect } from 'react'
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import ProfileContext from '../../../../context/ProfileContext';
import Tag from '../../../../components/Tag/index';

// api
import { getAllFavorite } from '../../../../api/favoriteApi';
import { updateUser } from '../../../../api/userApi';

export default function RelaInfo(props) {
    const profileContext = useContext(ProfileContext);
    const profileData = profileContext.dataProfile;
    const [arrFavorites, setArrFavorites] = useState([]); // favorite của user
    const [allFavorite, setAllFavorite] = useState([]); // tất cả favorite

    // add or remove tag in arrTag
    const handleClickTag = (fvrId) => {
        if (arrFavorites.includes(fvrId)) {
            setArrFavorites(arrFavorites.filter(item => item !== fvrId));
        } else {
            setArrFavorites(oldArray => [...oldArray, fvrId]);
        }
    }

    const getFavorites = async () => {
        await getAllFavorite()
            .then((res) => {
                console.log("get list favorite: ", res);
                if (res.status === 200) {
                    setAllFavorite(res.data.data);
                    // gán những favorite của user vào arrFavorites
                    profileData.favorites.map((fvr) => {
                        setArrFavorites(oldArray => [...oldArray, fvr._id]);
                    });
                }
            })
            .catch((err) => {
                console.log("get list favorite error: ", err);
            })
    }
    const saveFavoriteInfo = async () => {
        await updateUser({favorites: arrFavorites})
            .then((res) => {
                console.log(res);
                if (res.status === 200) {
                    profileContext.setDataProfile(res.data.data);
                    profileContext.setProfilePage("profile-edit");
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }

    useEffect(() => {
        getFavorites();
    }, [])

    return (
        <React.Fragment>
            <DialogTitle id="draggable-dialog-title">
                <div className="d-flex justify-content-between">
                    Favorites
                    <button type="button" className="btn_text_cancel" onClick={() => {
                        profileContext.setProfilePage("profile-edit");
                    }}>
                        Cancel
                    </button>
                </div>
            </DialogTitle>
            <DialogContent>
                <div className="d-flex flex-wrap">
                    {allFavorite &&
                        allFavorite.map((fvr) => (
                            <Tag
                                bgColor={arrFavorites.includes(fvr._id) ? "#FFA500" : "#C4C4C4"}
                                textColor={arrFavorites.includes(fvr._id) ? "#FFF" : "#000"}
                                text={fvr.name}
                                key={fvr._id}
                                onClick={e => handleClickTag(fvr._id)}
                            />
                        ))

                    }
                </div>
            </DialogContent>
            <DialogActions style={{ justifyContent: "center" }}>
                <div className="profile-edit-btn-save mt-2">
                    <button type="button" onClick={saveFavoriteInfo}>
                        SAVE
                    </button>
                </div>
            </DialogActions>
        </React.Fragment>
    )
}