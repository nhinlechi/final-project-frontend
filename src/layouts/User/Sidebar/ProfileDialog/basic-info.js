import React, { useContext, useState } from 'react';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import ProfileContext from '../../../../context/ProfileContext';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';

import DateFnsUtils from '@date-io/date-fns';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';

// api
import { updateUser } from '../../../../api/userApi';

const arrayGender = [
    {
        value: 0,
        label: 'Unknow',
    },
    {
        value: 1,
        label: 'Male',
    },
    {
        value: 2,
        label: 'Female',
    },
    {
        value: 3,
        label: 'Other',
    },
];

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    }
}));


export default function BasicInfo(props) {
    const classes = useStyles();
    const profileContext = useContext(ProfileContext);
    const profileData = profileContext.dataProfile;
    const [gender, setGender] = useState(profileData.gender);
    const [firstName, setFirstName] = useState(profileData.firstname);
    const [lastName, setLastName] = useState(profileData.lastname);
    const [selectedDate, setSelectedDate] = useState(new Date(profileData.dateofbirth));

    const [errorMsg, setErrorMsg] = useState('');

    const handleChangeGender = (event) => {
        setGender(event.target.value);
    };

    const handleDateChange = (date) => {
        setSelectedDate(date);
    };

    const saveBasicInfo = async () => {
        // validate
        if (firstName === '') {
            setErrorMsg('Please enter first name!');
            return;
        } else if (lastName === '') {
            setErrorMsg('Please enter last name!');
            return;
        } else {
            setErrorMsg('');
        }
        // submit, set 
        let data = {
            firstname: firstName,
            lastname: lastName,
            gender: gender,
            dateofbirth: selectedDate.getTime()
        };
        await updateUser(data)
            .then((res) => {
                console.log(res);
                if (res.status === 200) {
                    console.log("before", profileData);
                    console.log("after", res.data.data);
                    profileContext.setDataProfile(res.data.data);
                    profileContext.setProfilePage("profile-edit");
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }

    return (
        <React.Fragment>
            <DialogTitle id="draggable-dialog-title">
                <div className="d-flex justify-content-between">
                    Basic information
                    <button type="button" className="btn_text_cancel" onClick={() => {
                        profileContext.setProfilePage("profile-edit");
                    }}>
                        Cancel
                    </button>
                </div>
            </DialogTitle>
            <DialogContent>
                <form className={classes.root} noValidate autoComplete="off">
                    {errorMsg !== ''
                        ? <div style={{ color: 'red' }}>{errorMsg}</div>
                        : null
                    }
                    <TextField
                        id="first_name"
                        label="First Name"
                        style={{ margin: '8px 0px' }}
                        placeholder="Enter your first name"
                        fullWidth
                        margin="normal"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                        size="small"
                        defaultValue={firstName}
                        onChange={e => setFirstName(e.target.value)}
                    />
                    <TextField
                        id="last_name"
                        label="Last Name"
                        style={{ margin: '8px 0px' }}
                        placeholder="Enter your last name"
                        fullWidth
                        margin="normal"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                        size="small"
                        defaultValue={lastName}
                        onChange={e => setLastName(e.target.value)}
                    />
                    <TextField
                        id="gender"
                        select
                        label="Gender"
                        style={{ margin: '8px 0px' }}
                        fullWidth
                        value={gender}
                        onChange={handleChangeGender}
                        variant="outlined"
                        size="small"
                    >
                        {arrayGender.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                    
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <KeyboardDatePicker
                            margin="normal"
                            id="date-picker-dialog"
                            label="Birthday"
                            format="dd/MM/yyyy"
                            value={selectedDate}
                            fullWidth
                            onChange={handleDateChange}
                            KeyboardButtonProps={{
                                'aria-label': 'change date',
                            }}
                            inputVariant="outlined"
                            size="small"
                        />
                    </MuiPickersUtilsProvider>
                </form>
            </DialogContent>
            <DialogActions style={{ justifyContent: "center" }}>
                <div className="profile-edit-btn-save mt-2">
                    <button type="button" onClick={saveBasicInfo}>
                        SAVE
                    </button>
                </div>
            </DialogActions>
        </React.Fragment>
    )
}