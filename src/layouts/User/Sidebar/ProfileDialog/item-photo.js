import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


export default function ItemPhoto(props) {

    return (
        <div className="item-photo-container col">
            {props.link
                ?
                <React.Fragment>
                    <img src={props.link} alt="img-profile" className="item-photo" />
                    <button className="btn-add-photo" >
                        <FontAwesomeIcon icon="times" size="1x" onClick={props.clickDelete}/>
                    </button>
                </React.Fragment>
                :
                <React.Fragment>
                    <div className="item-photo"></div>
                    <button className="btn-add-photo" onClick={props.clickUpload}>
                        <FontAwesomeIcon icon="plus" size="1x" />
                    </button>
                </React.Fragment>
            }
        </div>
    )
}