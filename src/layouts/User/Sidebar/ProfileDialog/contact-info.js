import React, { useContext, useState } from 'react'
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import ProfileContext from '../../../../context/ProfileContext';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

// api
import { updateUser } from '../../../../api/userApi';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    }
}));
const REGEX_VALID_PHONE = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

export default function ContactInfo(props) {
    const classes = useStyles();
    const profileContext = useContext(ProfileContext);
    const profileData = profileContext.dataProfile;
    const [phoneNumber, setPhoneNumber] = useState(profileData.phone);
    const [errorMsg, setErrorMsg] = useState('');

    const handleChangePhone = (event) => {
        setPhoneNumber(event.target.value);
    };

    const saveContactInfo = async () => {
        // validate
        if (phoneNumber === '') {
            setErrorMsg('Please enter phone number!');
            return;
        } else if (!REGEX_VALID_PHONE.test(phoneNumber)) {
            setErrorMsg('Invalid phone number!');
            return;
        } else {
            setErrorMsg('');
        }
        // submit, set 
        await updateUser({phone: phoneNumber})
            .then((res) => {
                console.log(res);
                if (res.status === 200) {
                    profileContext.setDataProfile(res.data.data);
                    profileContext.setProfilePage("profile-edit");
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }

    return (
        <React.Fragment>
            <DialogTitle id="draggable-dialog-title">
                <div className="d-flex justify-content-between">
                    Contact information
                    <button type="button" className="btn_text_cancel" onClick={() => {
                        profileContext.setProfilePage("profile-edit");
                    }}>
                        Cancel
                    </button>
                </div>
            </DialogTitle>
            <DialogContent>
                <form className={classes.root} noValidate autoComplete="off">
                    {errorMsg !== ''
                        ? <div style={{ color: 'red' }}>{errorMsg}</div>
                        : null
                    }
                    <TextField
                        id="phone_number"
                        label="Phone number"
                        style={{ margin: '8px 0px' }}
                        placeholder="Enter your phone number"
                        fullWidth
                        margin="normal"
                        InputLabelProps={{
                            shrink: true,
                        }}
                        variant="outlined"
                        size="small"
                        defaultValue={phoneNumber}
                        onChange={handleChangePhone}
                    />
                </form>
            </DialogContent>
            <DialogActions style={{ justifyContent: "center" }}>
                <div className="profile-edit-btn-save mt-2">
                    <button type="button" onClick={saveContactInfo}>
                        SAVE
                    </button>
                </div>
            </DialogActions>
        </React.Fragment>
    )
}