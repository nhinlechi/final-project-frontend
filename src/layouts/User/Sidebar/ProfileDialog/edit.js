import React, { useContext, useRef } from 'react';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import ProfileContext from '../../../../context/ProfileContext';
import Avatar from '@material-ui/core/Avatar';
import AccountBox from '@material-ui/icons/AccountBox';
import CakeIcon from '@material-ui/icons/Cake';
import PhoneIcon from '@material-ui/icons/Phone';
import Tag from '../../../../components/Tag/index';
import ItemPhoto from './item-photo';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// api
import { updateUser } from '../../../../api/userApi';
import { getSignUrl, uploadFileNew } from '../../../../api/storageApi';

function renderSwitchGender(param) {
    switch (param) {
        case 0:
            return 'Unknow';
        case 1:
            return 'Nam';
        case 2:
            return 'Nữ';
        default:
            return 'Khác';
    };
}

function renderSwitchRelation(param) {
    switch (param) {
        case 0:
            return 'Unknow';
        case 1:
            return 'In relationship';
        case 2:
            return 'Alone';
        default:
            return 'Divorcee';
    };
}

function renderConvertBirthday(timestamp) {
    const d = new Date(timestamp);
    return (d.getDate() + '/' + (d.getMonth()+1) + '/' + d.getFullYear());
}

async function checkFileImage(fileType) {
    return await getSignUrl({type: fileType})
}

async function uploadFileImage(url, file) {
    return await uploadFileNew(url, file);
}

async function updateProfile(data) {
    return await updateUser(data);
}

export default function ProfileEdit(props) {
    const profileContext = useContext(ProfileContext);
    const dataProfile = profileContext.dataProfile;
    const inputFile = useRef(null);
    var arrayAvatar = dataProfile.avatar;

    const LoadArrayImage = () => {
        let arrTemp = [];
        let numberImage = dataProfile.avatar.length;
        // Hiển thị mặc định là 6 hình, nếu profile k đủ 6 hình thì load default
        dataProfile.avatar.map((link) => {
            arrTemp.push(link);
        })
        if (numberImage < 6) {
            for (var i = numberImage; i < 6; i++) {
                arrTemp.push("");
            }
        }
        return (
            arrTemp.map((link, index) => (
                <ItemPhoto key={"avt-edit-profile"+index}
                    link={link} 
                    clickUpload={onBtnAddImgClick} 
                    clickDelete={() => onBtnDelImgClick(link)}
                />
            ))
        )
    }

    // xử lý upload ảnh
    const handleFileUpload = async (e) => {
        const { files } = e.target;
        if (files && files.length) {
            const fileType = files[0].type;
            // check type image
            const check = await checkFileImage(fileType);
            if (check !== null && check.status === 200) {
                const data = check.data.data;
                // upload
                const upload = await uploadFileImage(data.uploadurl, files[0]);
                console.log("upload:", upload);
                if (upload !== null && upload.status === 200) {
                    arrayAvatar.push(data.accessurl);
                    const update = await updateProfile({
                        avatar: arrayAvatar
                    });
                    if (update !== null && update.status === 200) {
                        // set tạm
                        profileContext.setDataProfile({
                            ...dataProfile,
                            avatar: arrayAvatar
                        });
                        
                    }
                }
            }

        }
    }

    // xử lý delete ảnh
    const onBtnDelImgClick = async (link) => {
        arrayAvatar = arrayAvatar.filter(function(item) {
            return item !== link
        });

        const del = await updateProfile({avatar: arrayAvatar});
        if (del !== null && del.status === 200) {
            // set tạm
            profileContext.setDataProfile({
                ...dataProfile,
                avatar: arrayAvatar
            });
        }
    }

    // click button upload ảnh 
    const onBtnAddImgClick = () => {
        inputFile.current.click();
    }

    const submitUpdate = () => {
        profileContext.setProfilePage("profile-detail");
    }

    return (
        <React.Fragment>
            <DialogContent>
                <div className="profile-edit">
                    <div className="profile-edit-photo">
                        <div className="row list-photo">
                            <LoadArrayImage />
                        </div>
                        <div className="btn-upload-photo mt-3">
                            <input
                                style={{ display: "none" }}
                                // accept=".zip,.rar"
                                ref={inputFile}
                                onChange={handleFileUpload}
                                type="file"
                            />
                            <button type="button" onClick={onBtnAddImgClick}>
                                Add media
                            </button>
                        </div>
                    </div>
                    <div className="profile-edit-info">
                        <div className="profile mt-2">
                            <div className="profile-basic-info">
                                <div className="profile-basic-info-top d-flex justify-content-between">
                                    <div className="profile-text_22">
                                        Basic information
                                    </div>
                                    <button type="button" onClick={() => {
                                        profileContext.setProfilePage("profile-edit-basic-info")
                                    }}>
                                        <FontAwesomeIcon icon="edit" size="1x" />
                                    </button>
                                </div>

                                <div className="profile-content">
                                    <div className="card-mini-info">
                                        <div className="row mt-2">
                                            <div className="col-2 d-flex align-items-center">
                                                <Avatar>
                                                    <AccountBox />
                                                </Avatar>
                                            </div>
                                            <div className="col-10 d-flex flex-column" style={{ padding: 0 }}>
                                                <div className="text_20_b">{dataProfile.lastname + " " + dataProfile.firstname}</div>
                                                <div>Full name</div>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-2 d-flex align-items-center">
                                                <Avatar>
                                                    <AccountBox />
                                                </Avatar>
                                            </div>
                                            <div className="col-10 d-flex flex-column" style={{ padding: 0 }}>
                                                <div className="text_20_b">
                                                    {renderSwitchGender(dataProfile.gender)}
                                                </div>
                                                <div>Gender</div>
                                            </div>
                                        </div>
                                        <div className="row mt-2">
                                            <div className="col-2 d-flex align-items-center">
                                                <Avatar>
                                                    <CakeIcon />
                                                </Avatar>
                                            </div>
                                            <div className="col-10 d-flex flex-column" style={{ padding: 0 }}>
                                                <div className="text_20_b">
                                                    {renderConvertBirthday(dataProfile.dateofbirth)}
                                                </div>
                                                <div>Birthday</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="profile-hobbies">
                                <div className="profile-hobbies-top d-flex justify-content-between">
                                    <div className="profile-text_22">
                                        Favorites
                                    </div>
                                    <button type="button" onClick={() => {
                                        profileContext.setProfilePage("profile-edit-favorite-info")
                                    }}>
                                        <FontAwesomeIcon icon="edit" size="1x" />
                                    </button>
                                </div>
                                <div className="profile-content">
                                    {dataProfile.favorites.length > 0
                                        ?
                                        dataProfile.favorites.map((fvr) => (
                                            <Tag key={"fvr-" + fvr._id} bgColor={"#858585"} text={fvr.name} textColor={"#FFF"} />
                                        ))
                                        : null
                                    }
                                </div>
                            </div>
                            <div className="profile-contact">
                                <div className="profile-contact-top d-flex justify-content-between">
                                    <div className="profile-text_22">
                                        Contact information
                                    </div>
                                    <button type="button" onClick={() => {
                                        profileContext.setProfilePage("profile-edit-contact-info")
                                    }}>
                                        <FontAwesomeIcon icon="edit" size="1x" />
                                    </button>
                                </div>
                                <div className="profile-content">
                                    <div className="card-mini-info">
                                        <div className="row mt-2">
                                            <div className="col-2 d-flex align-items-center">
                                                <Avatar>
                                                    <PhoneIcon />
                                                </Avatar>
                                            </div>
                                            <div className="col-10 d-flex flex-column" style={{ padding: 0 }}>
                                                <div className="text_20_b">{dataProfile.phone}</div>
                                                <div>Phone number</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="profile-relationship">
                                <div className="profile-relationship-top d-flex justify-content-between">
                                    <div className="profile-text_22">
                                        Relationship status
                                    </div>
                                    <button type="button" onClick={() => {
                                        profileContext.setProfilePage("profile-edit-rela-info")
                                    }}>
                                        <FontAwesomeIcon icon="edit" size="1x" />
                                    </button>
                                </div>
                                <div className="profile-content">
                                    <div className="card-mini-info">
                                        <div className="row mt-2">
                                            <div className="col-2 d-flex align-items-center">
                                                <Avatar>
                                                    <AccountBox />
                                                </Avatar>
                                            </div>
                                            <div className="col-10 d-flex flex-column" style={{ padding: 0 }}>
                                                <div className="text_20_b">
                                                    {renderSwitchRelation(dataProfile.relationship)}
                                                </div>
                                                <div>Relationship</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </DialogContent>
            <DialogActions style={{ justifyContent: "center" }}>
                <div className="profile-edit-btn-save mt-2" onClick={submitUpdate}>
                    <button type="button">
                        DONE
                    </button>
                </div>
            </DialogActions>
        </React.Fragment>
    )
}