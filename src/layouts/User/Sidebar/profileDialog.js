import React, { useContext } from 'react';

import ProfileContext from '../../../context/ProfileContext';
import Dialog from '@material-ui/core/Dialog';
import ProfileDetail from './ProfileDialog/detail';
import ProfileEdit from './ProfileDialog/edit';

import ProfileEditBasicInfo from './ProfileDialog/basic-info';
import ProfileEditContactInfo from './ProfileDialog/contact-info';
import ProfileEditRelaInfo from './ProfileDialog/rela-info';
import ProfileEditFavorite from './ProfileDialog/favorite-info';

export default function ProfileDialog(props) {
    const profileContext = useContext(ProfileContext);
    const profilePage = profileContext.profilePage;

    const handleCloseProfile = () => {
        props.setOpen(false);
        profileContext.setProfilePage("profile-detail");
    };

    return (
        <React.Fragment>
            <Dialog
                open={props.open}
                onClose={handleCloseProfile}
                scroll="paper"
                maxWidth="xs"
                aria-labelledby="scroll-dialog-title"
                aria-describedby="scroll-dialog-description"
                fullWidth
            >
            {
                profilePage === "profile-detail" ? (
                    <ProfileDetail />
                ) : profilePage === "profile-edit" ? (
                    <ProfileEdit />
                ) : profilePage === "profile-edit-basic-info" ? (
                    <ProfileEditBasicInfo />
                ) : profilePage === "profile-edit-contact-info" ? (
                    <ProfileEditContactInfo />
                ) : profilePage === "profile-edit-rela-info" ? (
                    <ProfileEditRelaInfo />
                ) : profilePage === "profile-edit-favorite-info" ? (
                    <ProfileEditFavorite />
                ) : null
            }
            </Dialog>
        </React.Fragment >
    );
}
