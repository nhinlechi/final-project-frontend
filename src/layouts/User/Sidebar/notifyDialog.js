import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";

import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";

import Avatar from "@material-ui/core/Avatar";
import NotificationItem from "../../../components/NotificationItem/index";

import { getAllNotification } from "../../../api/notificationApi";
import { getUserById } from "../../../api/userApi";
import { getGroupById } from "../../../api/groupApi";
import { DialogActions } from "@material-ui/core";
import {confirmJoinGroup} from "../../../api/matchApi"

export default function NotifyDialog(props) {
  const [dataNotify, setDataNotify] = useState([]);
  const [confirmRequestJoinGroup, setConfirmRequestJoinGroup] = useState({});
  const [isOpenDialogConfirm, setIsOpenDialogConfirm] = useState(false);
  const handleCloseNotify = () => {
    props.setOpen(false);
  };

  const [userInfor, setUserInfor] = useState({
    firstname: "",
    lastname: "",
    avatar: [],
  });
  const [groupInfor, setGroupInfor] = useState({ name: "", photos: [] });

  useEffect(() => {
    getAllNotification(1, 10)
      .then((res) => {
        if (res.status === 200) {
          setDataNotify(res.data.data.notification);
          console.log(res);
        }
      })
      .catch((err) => {
        console.log(err.message);
      });
  }, [props.open]);

  useEffect(() => {
    if (isOpenDialogConfirm) {
      getUserById(confirmRequestJoinGroup.userid).then((res) => {
        setUserInfor(res.data.data);
      });
      getGroupById(confirmRequestJoinGroup.serviceid).then((res) => {
        setGroupInfor(res.data.data);
      });
    }
  }, [isOpenDialogConfirm]);

  const handleConfirmJoinGroup = (status) =>{
    confirmJoinGroup({status:status, notificationid:confirmRequestJoinGroup.notificationid}).then((res)=>{
      setIsOpenDialogConfirm(false);
      handleCloseNotify();
    })
  }

  return (
    <div>
      {!isOpenDialogConfirm ? (
        <Dialog
          open={props.open}
          onClose={handleCloseNotify}
          scroll="paper"
          fullWidth={true}
          maxWidth="xs"
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <DialogTitle id="scroll-dialog-title" className="noti-title">
            Notifications
          </DialogTitle>
          <DialogContent>
            <div className="notification">
              <div className="noti-content">
                {dataNotify &&
                  dataNotify.map((notification) => (
                    <div className="noti-item" key={notification._id}>
                      <NotificationItem
                        notificationItem={notification}
                        closeNotify={handleCloseNotify}
                        setOpenConfirm={setIsOpenDialogConfirm}
                        setConfirmRequestJoinGroup={setConfirmRequestJoinGroup}
                      />
                    </div>
                  ))}
              </div>
            </div>
          </DialogContent>
        </Dialog>
      ) : (
        <Dialog
          open={props.open}
          onClose={handleCloseNotify}
          scroll="paper"
          fullWidth={true}
          maxWidth="xs"
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <DialogTitle id="scroll-dialog-title" className="noti-title">
            Notifications
          </DialogTitle>
          <DialogContent>
            <div className="notification">
              <div className="noti-content">{`${userInfor.firstname} ${userInfor.lastname} muốn tham gia nhóm ${groupInfor.name}`}</div>
            </div>
          </DialogContent>
          <DialogActions>
            <Button
              autoFocus
              onClick={
                ()=>{
                  handleConfirmJoinGroup(false);
                }
              }
              color="primary"
            >
              Reject
            </Button>
            <Button
              color="btn_ffa500"
              onClick={() => {
                handleConfirmJoinGroup(true);
              }}
            >
              Confirm
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </div>
  );
}
