import axios from 'axios';

export const getUserInfo = async () => {
    const url = process.env.REACT_APP_API_BASE_URL + '/user';
    const token = localStorage.getItem(process.env.REACT_APP_ACCESS_TOKEN_NAME)
    let params = {
        "select[]": [
            "favorites", "occupations", "listliked", "friends", "historymatch"
        ]
    }
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'auth-token': token
        },
        params: params
    }
    const res = await axios.get(url, config);
    return res;
};

export const getUserById = async (id) => {
    const url = process.env.REACT_APP_API_BASE_URL + '/user/' + id;
    const token = localStorage.getItem(process.env.REACT_APP_ACCESS_TOKEN_NAME);
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'auth-token': token
        }
    };
    const res = await axios.get(url, config);
    return res;
}

// cập nhật profile
export const updateUser = async (data) => {
    const url = process.env.REACT_APP_API_BASE_URL + '/user/update';
    const token = localStorage.getItem(process.env.REACT_APP_ACCESS_TOKEN_NAME);
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'auth-token': token
        }
    };
    const res = await axios.patch(url, data, config);
    return res;
}

// forgot password: quên mật khẩu
export const forgotPasswordUser = async (data) => {
    const url = process.env.REACT_APP_API_BASE_URL + '/user/forgotpassword';
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    const res = await axios.post(url, data, config);
    return res;
}

// reset password: reset mật khẩu
export const resetPasswordUser = async (data) => {
    const url = process.env.REACT_APP_API_BASE_URL + '/user/resetnewpassword';
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    const res = await axios.put(url, data, config);
    return res;
}

// reset password: reset mật khẩu
export const changePasswordUser = async (data) => {
    const url = process.env.REACT_APP_API_BASE_URL + '/user/changpassword';
    const config = {
        headers: {
            'Content-Type': 'application/json'
        }
    };
    const res = await axios.put(url, data, config);
    return res;
}