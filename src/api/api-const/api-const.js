const APIPath = {
    REQUEST_MATCH_NOW: "/match/matchnow",
    CANCEL_MATCH_REQUEST_MATCH_NOW: "/match/cancelrequest",
    CONFIRM_MATCH: "/match/confirmmatch",
    GET_ALL_MATCH: "/match",
    GET_CHAT: "/match/chat",
    REQUEST_MATCH_LATER: "/match/matchlater",
    GET_RECOMMEND_PLACE: "/match/recommendplace",
    SEND_PLACE: "/match/sendPlace",
    CONFIRM_REQUEST_PLACE: "/match/confirmPlace",
    CANCEL_REQUEST_PLACE: "/match/deletePlace",
    CONFIRM_REQUEST_JOIN: "/group/confirmjoin",
    GET_PLACE_VIEWPORT: "/place/getplacesinviewport",
    GET_MEETING_HISTORY: "/match/meetinghistory",
}

export default APIPath;