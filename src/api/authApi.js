import axios from 'axios';

// login google
export const loginGoogle = async (data) => {
    const url = process.env.REACT_APP_API_BASE_URL + '/auth/logingoogle';
    const config = {
        headers: {
            'Content-Type': 'application/json',
        }
    }
    const res = await axios.post(url, data, config);
    return res;
}

// login facebook
export const loginFacebook = async (data) => {
    const url = process.env.REACT_APP_API_BASE_URL + '/auth/loginfacebook';
    const config = {
        headers: {
            'Content-Type': 'application/json',
        }
    }
    const res = await axios.post(url, data, config);
    return res;
}

// register
export const registerUser = async (data) => {
    const url = process.env.REACT_APP_API_BASE_URL + '/auth/register';
    const config = {
        headers: {
            'Content-Type': 'application/json',
        }
    }
    const res = await axios.post(url, data, config);
    return res;
}

// login
export const loginWithEmail = async (data) => {
    const url = process.env.REACT_APP_API_BASE_URL + '/auth/login';
    const config = {
        headers: {
            'Content-Type': 'application/json',
        }
    }
    const res = await axios.post(url, data, config);
    return res;
}