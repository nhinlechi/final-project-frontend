import axios from 'axios';

export const saveFirebase = async (data) => {
    const token = localStorage.getItem(process.env.REACT_APP_ACCESS_TOKEN_NAME);
    const url = process.env.REACT_APP_API_BASE_URL + '/firebase';
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'auth-token': token
        }
    };
    const res = await axios.post(url, data, config);
    return res;
}