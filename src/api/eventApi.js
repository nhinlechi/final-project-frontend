import axios from 'axios';

// Create new event (thêm event)
export const createEvent = async (data, groupId) => {
    const token = localStorage.getItem(process.env.REACT_APP_ACCESS_TOKEN_NAME);
    const url = process.env.REACT_APP_API_BASE_URL + '/group/' + groupId + '/event';
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'auth-token': token
        }
    }
    const res = await axios.post(url, data, config);
    return res;
}

// Update event (cập nhật event)
export const updateEvent = async (data, groupId) => {
    console.log(data);
    const token = localStorage.getItem(process.env.REACT_APP_ACCESS_TOKEN_NAME);
    const url = process.env.REACT_APP_API_BASE_URL + '/group/' + groupId + '/event';
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'auth-token': token
        }
    }
    const res = await axios.patch(url, data, config);
    return res;
}

// Send request to join event (Gửi yêu cầu tham gia event)
export const sendReqJoinEvent = async (data, groupId) => {
    const token = localStorage.getItem(process.env.REACT_APP_ACCESS_TOKEN_NAME);
    console.log(data);
    let params = {
        groupid: groupId
    };
    const url = process.env.REACT_APP_API_BASE_URL + '/group/' + groupId + '/event/join';
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'auth-token': token
        },
        params
    }
    const res = await axios.post(url, data, config);
    return res;
}

// get list event (lấy danh sách event theo filter)
export const getListEvent = async (params, groupId) => {
    const token = localStorage.getItem(process.env.REACT_APP_ACCESS_TOKEN_NAME);
    const url = process.env.REACT_APP_API_BASE_URL + '/group/' + groupId + '/event';
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'auth-token': token
        },
        params
    }
    const res = await axios.get(url, config);
    return res;
}

// Delete event (xoá event)
export const deleteEvent = async (params, groupId) => {
    const token = localStorage.getItem(process.env.REACT_APP_ACCESS_TOKEN_NAME);
    const url = process.env.REACT_APP_API_BASE_URL + '/group/' + groupId + '/event';
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'auth-token': token
        },
        params
    }
    const res = await axios.delete(url, config);
    return res;
}

// detail event
export const getEventById = async (groupId, eventId) => {
    const token = localStorage.getItem(process.env.REACT_APP_ACCESS_TOKEN_NAME);
    const url = process.env.REACT_APP_API_BASE_URL + '/group/' + groupId + '/event/' + eventId;
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'auth-token': token
        }
    }
    const res = await axios.get(url, config);
    return res;
}

// get chat event
export const getChatEvent = async (params) => {
    const token = localStorage.getItem(process.env.REACT_APP_ACCESS_TOKEN_NAME);
    const url = process.env.REACT_APP_API_BASE_URL + '/group/' + params.groupid + '/event/chat';
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'auth-token': token
        },
        params
    }
    const res = await axios.get(url, config);
    return res;
}