import APIPath from './api-const/api-const';
import AxiosIntance from './axios-config/axiosCofig';

/**
 * 
 * @param {Object} data match now body
 * @param {number} data.lat
 * @param {number} data.long 
 * @param {number} data.gender 
 * @param {number} data.relationship 
 * @param {number} data.distance 
 * @param {number} data.maxage
 * @param {number} data.minage 
 * @returns {jons}  match now response
 */
export const matchNow = async (data) => {
    const axiosIntance = AxiosIntance();
    const res = await axiosIntance.post(APIPath.REQUEST_MATCH_NOW, data);
    return res;
}

/**
 * Cancel request match now and match late
 * @returns cancel request match response
 */
export const cancelRequestMatch = async () => {
    const axiosIntance = AxiosIntance();
    const res = await axiosIntance.post(APIPath.CANCEL_MATCH_REQUEST_MATCH_NOW);
    return res;
}

/**
 * Confirm match (now and later)
 * @param {Object} data 
 * @param {boolean} data.status
 * @param {string} data.id
 * @returns Response confirm match
 */
export const confirmRequestMatch = async (data) => {
    const axiosIntance = AxiosIntance();
    const res = await axiosIntance.patch(APIPath.CONFIRM_MATCH, data);
    return res;
}

export const getAllMatch = async () => {
    const axiosIntance = AxiosIntance();
    const res = await axiosIntance.get(APIPath.GET_ALL_MATCH);
    return res;
}


export const getChat = async (params) => {
    const axiosIntance = AxiosIntance();
    const res = await axiosIntance.get(APIPath.GET_CHAT, { params: params });
    return res;
}

export const requestMatchLater = async (body) =>{
    const axiosIntance = AxiosIntance();
    const res  = await axiosIntance.post(APIPath.REQUEST_MATCH_LATER, body);
    return res;
}

export const getRecommendPlace = async (params) =>{
    const axiosIntance = AxiosIntance();
    const res = await axiosIntance.get(`${APIPath.GET_RECOMMEND_PLACE}/${params.id}`);
    return res;
}

export const sendPlace = async (body) => {
    const axiosIntance = AxiosIntance();
    const res = await axiosIntance.post(APIPath.SEND_PLACE, body);
    return res;
}

export const confirmPlace = async (body) =>{
    const axiosIntance = AxiosIntance();
    const res = await axiosIntance.post(APIPath.CONFIRM_REQUEST_PLACE, body);
    return res;
}



export const deletePlace = async (body) =>{
    const axiosIntance = AxiosIntance();
    const res = await axiosIntance.delete(APIPath.CANCEL_REQUEST_PLACE, {data: body});
    return res;
}

export const confirmJoinGroup = async (body) =>{
    const axiosIntance = AxiosIntance();
    const res = await axiosIntance.post(APIPath.CONFIRM_REQUEST_JOIN, body);
    return res;
}

export const getMeetingHistory = async (params) =>{
    const axiosIntance = AxiosIntance();
    const res = await axiosIntance.get(APIPath.GET_MEETING_HISTORY, {params:params});
    return res;
}

