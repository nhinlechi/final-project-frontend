import axios from 'axios';

// Get all group tag
export const getAllTag = async (params) => {
    const token = localStorage.getItem(process.env.REACT_APP_ACCESS_TOKEN_NAME);
    const url = process.env.REACT_APP_API_BASE_URL + '/tag';
    const config = {
        headers: {
            'Content-Type': 'application/json',
            'auth-token': token
        },
        params
    }
    const res = await axios.get(url, config);
    return res;
}