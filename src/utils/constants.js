export const restaurantTypes = [
    {key: "restaurant", value: "Restaurant"},
    {key: "bar", value: "Bar"},
    {key: "cafe", value: "Coffee"},
    {key: "shoping_mall", value: "Shopping mall"}
];

export const restaurantSorts = [
{key: "rating", value: "Rating"},
{key: "name", value: "Name (A - Z)"}
];

export const restaurantInfoTabs = [
    {index: 0, value: "Basic Infomation"},
    {index: 1, value: "Reviews"},
    {index: 2, value: "View on Map"}
]
