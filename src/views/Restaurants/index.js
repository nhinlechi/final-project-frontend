import React, { useContext } from "react";
import {
  RestaurantContext,
  RestaurantProvider,
} from "../../context/RestaurantContext";
import RestaurantList from "../../components/Restaurant/RestaurantList";
import RestaurantSearch from "../../components/Restaurant/RestaurantSearch";
import RestaurantInfo from "../../components/Restaurant/RestaurantInfo/RestaurantInfo";
import classes from "./Restaurants.module.css";

export default function Restaurant() {
  return <RestaurantChild />;
}

function RestaurantChild() {
  return (
    <React.Fragment>
      <RestaurantSearch />
      <hr
        style={{
          width: "1px",
          height: "auto",
          backgroundColor: "black",
          marginRight: "1rem",
        }}
      />
      <RestaurantList />
    </React.Fragment>
  );
}
