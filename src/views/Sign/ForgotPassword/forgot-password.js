import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import logo from '../../../assets/images/img.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ToastContainer, toast } from 'react-toastify';

// api
import { forgotPasswordUser } from '../../../api/userApi';

export default function ForgotPassword() {
    const history = useHistory();
    // validate
    const { register, errors, handleSubmit } = useForm();

    const onSubmit = async (formValues) => {
        await forgotPasswordUser({ email: formValues.email })
            .then((res) => {
                if (res.status === 200) {
                    // alert
                    toast.success('Please check email to reset new password!', {
                        position: "top-right",
                        autoClose: 3000,
                        hideProgressBar: true,
                        closeOnClick: false,
                        draggable: true,
                    });

                    // hành động
                    setTimeout(function(){
                        history.push('/reset-password');
                    }, 3000);
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="limiter">
                <div className="container-login100">
                    <div className="wrap-login100">
                        <div className="login100-pic js-tilt">
                            <img src={logo} alt="Logo" />
                        </div>
                        <div className="login100-form validate-form">
                            <span className="login100-form-title">
                                Forgot Password
                            </span>
                            {errors.email && (
                                <p className="errorMsg">{errors.email.message}</p>
                            )}
                            <div className="wrap-input100 validate-input">
                                <input className="input100" type="text" name="email" placeholder="Email"
                                    ref={register({
                                        required: 'Email is required!',
                                        pattern: {
                                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                            message: "Enter a valid email address",
                                        },
                                    })}
                                />
                                <span className="focus-input100"></span>
                                <span className="symbol-input100">
                                    <FontAwesomeIcon icon="envelope" />
                                </span>
                            </div>
                            <div className="container-login100-form-btn">
                                <button className="login100-form-btn" type="submit">
                                    SUBMIT
                                </button>
                            </div>
                            <div className="text-center">
                                <Link className="txt2" to="/login">
                                    Return to login
                                    <i className="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <ToastContainer />
        </form>
    )
}