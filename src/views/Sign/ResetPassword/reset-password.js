import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useForm } from 'react-hook-form';
import logo from '../../../assets/images/img.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { ToastContainer, toast } from 'react-toastify';

// api
import { resetPasswordUser } from '../../../api/userApi';

export default function ResetPassword() {
    const history = useHistory();
    // validate
    const { register, errors, handleSubmit } = useForm();
    // state
    const [errorMsg, setErrorMsg] = React.useState('');

    const onSubmit = async (formValues) => {
        // check password vs confirm password
        if (formValues.new_password !== formValues.confirm_new_password) {
            setErrorMsg('Confirm password must be password!');
            return
        } else {
            setErrorMsg('');
        }

        let data = {
            email: formValues.email,
            code: formValues.code,
            newpassword: formValues.new_password,
            confirmnewpassword: formValues.confirm_new_password
        }
        await resetPasswordUser(data)
            .then((res) => {
                if (res.status === 200) {
                    // alert
                    toast.success('Reset password success!', {
                        position: "top-right",
                        autoClose: 1000,
                        hideProgressBar: true,
                        closeOnClick: true,
                        draggable: true,
                    });

                    // hành động
                    setTimeout(function(){
                        history.push('/login');
                    }, 1000);
                }
            })
            .catch((err) => {
                console.log(err);
            })
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className="limiter">
                <div className="container-login100">
                    <div className="wrap-login100">
                        <div className="login100-pic js-tilt">
                            <img src={logo} alt="Logo" />
                        </div>
                        <div className="login100-form validate-form">
                            <span className="login100-form-title">
                                Reset Password
                            </span>

                            {errorMsg === ''
                                ? <p className="errorMsg">{errorMsg}</p>
                                : null
                            }
                            {errors.email && (
                                <p className="errorMsg">{errors.email.message}</p>
                            )}
                            <div className="wrap-input100 validate-input">
                                <input className="input100" type="text" name="email" placeholder="Email"
                                    ref={register({
                                        required: 'Email is required!',
                                        pattern: {
                                            value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
                                            message: "Enter a valid email address",
                                        },
                                    })}
                                />
                                <span className="focus-input100"></span>
                                <span className="symbol-input100">
                                    <FontAwesomeIcon icon="envelope" />
                                </span>
                            </div>
                            {errors.code && (
                                <p className="errorMsg">{errors.code.message}</p>
                            )}
                            <div className="wrap-input100 validate-input">
                                <input className="input100" type="text" name="code" placeholder="Code"
                                    ref={register({
                                        required: 'Code is required!',
                                    })}
                                />
                                <span className="focus-input100"></span>
                                <span className="symbol-input100">
                                    <FontAwesomeIcon icon="info-circle" />
                                </span>
                            </div>
                            {errors.new_password && (
                                <p className="errorMsg">{errors.new_password.message}</p>
                            )}
                            <div className="wrap-input100 validate-input">
                                <input className="input100" type="password" name="new_password" placeholder="New password"
                                    ref={register({
                                        required: 'New password is required!',
                                        minLength: {
                                            value: 6,
                                            message: 'Password must be at least 6 characters'
                                        },
                                        maxLength: {
                                            value: 30,
                                            message: 'Password up to 30 characters'
                                        }
                                    })}
                                />
                                <span className="focus-input100"></span>
                                <span className="symbol-input100">
                                    <FontAwesomeIcon icon="lock" />
                                </span>
                            </div>

                            {errors.confirm_new_password && (
                                <p className="errorMsg">{errors.confirm_new_password.message}</p>
                            )}
                            <div className="wrap-input100 validate-input">
                                <input className="input100" type="password" name="confirm_new_password" placeholder="Confirm new password"
                                    ref={register({
                                        required: 'Confirm new password is required!',
                                        minLength: {
                                            value: 6,
                                            message: 'Password must be at least 6 characters'
                                        },
                                        maxLength: {
                                            value: 30,
                                            message: 'Password up to 30 characters'
                                        }
                                    })}
                                />
                                <span className="focus-input100"></span>
                                <span className="symbol-input100">
                                    <FontAwesomeIcon icon="lock" />
                                </span>
                            </div>

                            <div className="container-login100-form-btn">
                                <button className="login100-form-btn" type="submit">
                                    SUBMIT
                                </button>
                            </div>

                            <div className="text-center">
                                <Link className="txt2" to="/login">
                                    Return to login
                                    <i className="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                                </Link>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <ToastContainer />
        </form>
    )
}