import React, { useContext, useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useHistory, Route } from "react-router-dom";
// component
import ContactItem from "../../components/ContactItem/index";
import socketIOClient from "socket.io-client";
import PrivateMessage from "../../components/PrivateMessage/PrivateMessage";
import PageLoading from "../../components/PageLoading/pageLoading";
// API
import { getAllMatch } from "../../api/matchApi";
import { LoadingContext } from "../../context/LoadingContext";
import MatchContext from "../../context/MatchContext";
import classes from "./message.module.css"

export default function Message(props) {
  const [listMatch, setListMatch] = useState([]);
  const loadingContext = useContext(LoadingContext);
  const history = useHistory();
  const matchContext = useContext(MatchContext);
  const [isMessageItemActive, setIsMessageItemActive] = useState();
  const getAllMatchHanlder = async () => {
    await getAllMatch()
      .then((res) => {
        console.log(res.data.data);
        setListMatch(
          res.data.data.sort((a, b) => {
            let ac, bc;
            if (a.messages.length === 0) {
              ac = a.created;
            } else {
              const messages = a.messages;
              const lastMessagesData = messages[messages.length - 1];
              ac = lastMessagesData.created;
            }

            if (b.messages.length === 0) {
              bc = b.created;
            } else {
              const messages = b.messages;
              const lastMessagesData = messages[messages.length - 1];
              bc = lastMessagesData.created;
            }

            return bc - ac;
          })
        );
        loadingContext.setIsLoading(false);
      })
      .catch((err) => {
        console.error("Get all match: ", err);
      });
  };

  useEffect(() => {
    loadingContext.setIsLoading(true);
    getAllMatchHanlder();
  }, [matchContext.matchInfor]);

  const redirectMessagePrivateHandler = (matchId) => {
    history.push(`/messages/${matchId}`);
  };

  const listItemMessage = listMatch.map((element) => {
    const messages = element.messages;
    const lastMessagesData = messages[messages.length - 1];
    let lastMessage = { type: "text", message: "" };
    if (lastMessagesData)
      lastMessage = lastMessagesData.data[lastMessagesData.data.length - 1];
    return (
      <ContactItem
        key={element._id}
        isActive={isMessageItemActive}
        matchInfor={element}
        lastMsg={lastMessage}
        onClick={() => {
          redirectMessagePrivateHandler(element._id);
        }}
      />
    );
  });
  return (
    <React.Fragment>
      {/* Start general information MIDDLE */}
      {loadingContext.isLoading && <PageLoading />}
        <div className={`content scrollable ${classes.wrapper}`}>
              <div className="heading-filter">
                <h3 className="heading-filter__fs15" style={{paddingLeft:"1rem"}}>Chats</h3>
              </div>
            <div className="list-msg">{listItemMessage}</div>
          {/* <button onClick={test}>Test</button> */}
        </div>
        <hr style={{width:"1px", height:"auto", backgroundColor:"black", marginRight:"1rem"}}/>

      {/* Start content RIGHT */}
        {/* Header */}
        <div style={{width:"100%"}}>
        <Route path={`/messages/:matchId`}>
          <PrivateMessage
            listMatch={listMatch}
            setActive={setIsMessageItemActive}
          />
        </Route>
        </div>
      {/* End content RIGHT */}
      {/* End general information MIDDLE */}
    </React.Fragment>
  );
}
