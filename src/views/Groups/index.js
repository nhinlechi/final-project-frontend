import React from 'react';
import { Col } from 'react-bootstrap';
import { Route } from 'react-router-dom';
import { GroupProvider } from "../../context/GroupContext";

import GroupFilter from '../../components/Group/GroupFilter';
import GroupList from '../../components/Group/GroupList';
import GroupDetail from '../../components/Group/GroupDetail';
import GroupEdit from '../../components/Group/GroupEdit';
import GroupCreate from '../../components/Group/GroupCreate';

import EventDetail from '../../components/Group/Event/EventDetail';
import EventCreate from '../../components/Group/Event/EventCreate';
import EventEdit from '../../components/Group/Event/EventEdit';

import classes from './group.module.css'


export default function Group() {
    return (
        <GroupProvider>
            <GroupChild />
        </GroupProvider>
    );
}

function GroupChild(props) {
    return (
        <React.Fragment>
                <div className={classes.groupfilter}>
                    <GroupFilter />
                </div>
                <Route path={`/groups`} exact>
                    <GroupList />
                </Route>
                <Route path={`/groups/create`} exact>
                    <GroupCreate />
                </Route>
                <Route path={`/groups/detail/:groupId`} exact>
                    <GroupDetail />
                </Route>
                <Route path={`/groups/edit/:groupId`} exact>
                    <GroupEdit />
                </Route>

                {/* START EVENT */}
                
                <Route path={`/groups/:groupId/event/detail/:eventId`} exact>
                    <EventDetail />
                </Route>
                <Route path={`/groups/:groupId/event/edit/:eventId`} exact>
                    <EventEdit />
                </Route>
                <Route path={`/groups/:groupId/event/create`} exact>
                    <EventCreate />
                </Route>
                {/* END EVENT */}
        </React.Fragment>
    )
}
