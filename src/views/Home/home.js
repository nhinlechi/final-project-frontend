import React, { useContext, useState } from "react";
import classes from "./home.module.css";
// component
import Filter from "../../components/Filters/index";
import RequestMatch from "../../components/RequestMatch/index";
import Map from "../../components/Map/Map"
export default function Home(props) {

  const [isProcessMatching, setIsProcessMatching] = useState(false);
  const [userMatching, setUserMatching] = useState({});
  const [matchNowBody, setMatchNowBody] = useState({
    minage: 18,
    maxage: 90,
    distance: 5,
    gender: 1,
    relationship: 1,
  });

  const [isMatchLater, setIsMatchLater] = useState(false);
  console.log("Is match later", isMatchLater);
  /**
   * Get Match now body from filter
   * @param {Ojbect} matchNowBodyFilterData
   * @param {number} minage
   * @param {number} maxage
   * @param {number} distance
   * @param {number} gender
   * @param {number} relationship
   */
  const setMatchNowBodyFromFilter = (matchNowBodyFilterData) => {
    setMatchNowBody({
      minage: matchNowBodyFilterData.minage,
      maxage: matchNowBodyFilterData.maxage,
      distance: matchNowBodyFilterData.distance,
      gender: matchNowBodyFilterData.gender,
      relationship: matchNowBodyFilterData.relationship,
    });
  };

  const setUserMatchingHandler = (userMatched) => {
    if (userMatched)
      setUserMatching({
        fullname: userMatched.fullname,
        avatar: userMatched.avatar,
        notificationid: userMatched.notificationid,
        timeout: userMatched.timeout,
      });
  };



  return (
    <React.Fragment>
      <div className={classes.wrapper}>
        {/* Start general information MIDDLE */}
        <div className={classes.match}>
          <Filter
            setMatchNowBodyFromFilter={setMatchNowBodyFromFilter}
            matchNowDefaultValue={matchNowBody}
            setIsMatchLater={() => {
              setIsMatchLater(!isMatchLater);
            }}
          />
          <RequestMatch
            matchNowBody={matchNowBody}
            setIsProcessMatching={setIsProcessMatching}
            setUserMatchingHandler={setUserMatchingHandler}
            isMatchLater={isMatchLater}
          />
        </div>
        <Map/>
      </div>
    </React.Fragment>
  );
}
